package disabled.OK.reports;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractReportTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class GetNonExistentReportTestCase extends AbstractReportTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Reports.GET_REPORT_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> remoteDbClient.getReport(String.valueOf(resultIdProvider.getNextUniqueId())))
        );
    }
}
