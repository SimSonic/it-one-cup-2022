package disabled.OK.table_queries.non_existent;

import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;

import java.util.List;

// @Component
@RequiredArgsConstructor
@Slf4j
public class GetAllByNonExistentNameTableQueryTestCase extends AbstractTableQueryTestCase {

    private static final String NON_EXISTENT_TABLE_NAME = "abcdefghijklmnopqrstuvwxyz";

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        // TODO: Не реализовано?
                        "/api/table-query/add-get-all-queries-by-table-name-result",
                        HttpStatus.OK,
                        this::getQueryById)
        );
    }

    private ResponseEntity<?> getQueryById() {
        return remoteDbClient.getTableQuery(NON_EXISTENT_TABLE_NAME);
    }
}
