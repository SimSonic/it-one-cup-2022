package disabled.OK.table_queries.incorrect_id;

import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class DeleteIncorrectIdTableQueryTestCase extends AbstractTableQueryTestCase {

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.DELETE_TABLE_QUERY_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> remoteDbClient.deleteTableQuery("-1")),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.DELETE_TABLE_QUERY_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> remoteDbClient.deleteTableQuery("2147483648")),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.DELETE_TABLE_QUERY_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> remoteDbClient.deleteTableQuery("abcdefghijklmnopqrstuvwxyz"))
        );
    }
}
