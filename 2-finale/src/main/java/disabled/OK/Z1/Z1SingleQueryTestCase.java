package disabled.OK.Z1;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractSingleQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import one.util.streamex.IntStreamEx;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.ModifySingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Order(11)
@Component
@RequiredArgsConstructor
public class Z1SingleQueryTestCase extends AbstractSingleQueryTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        TestCaseStep testCaseStep = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.MODIFY_SINGLE_QUERY),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.modifySingleQuery(ModifySingleQueryRequestDto.builder()
                                                               .queryId((long) resultIdProvider.getNextUniqueId())
                                                               .query("SELECT * FROM a_hren_tebe")
                                                               .build()));
        return IntStreamEx.range(20)
                .mapToObj(i -> testCaseStep)
                .toList();
    }
}
