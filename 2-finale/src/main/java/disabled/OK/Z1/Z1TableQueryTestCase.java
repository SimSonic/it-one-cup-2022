package disabled.OK.Z1;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import one.util.streamex.IntStreamEx;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Order(31)
@Component
@RequiredArgsConstructor
public class Z1TableQueryTestCase extends AbstractTableQueryTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        TestCaseStep testCaseStep = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.executeTableQuery(String.valueOf(resultIdProvider.getNextUniqueId())));
        return IntStreamEx.range(20)
                .mapToObj(i -> testCaseStep)
                .toList();
    }
}
