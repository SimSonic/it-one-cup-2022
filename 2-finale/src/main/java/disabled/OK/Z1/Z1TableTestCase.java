package disabled.OK.Z1;

import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import lombok.RequiredArgsConstructor;
import one.util.streamex.IntStreamEx;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto.ColumnInfo;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Order(21)
@Component
@RequiredArgsConstructor
public class Z1TableTestCase extends AbstractTableTestCase {

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        TestCaseStep testCaseStep = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.createTable(CreateTableRequestDto.builder()
                                                         .tableName("customer%")
                                                         .columnsAmount(1)
                                                         .columnInfos(List.of(ColumnInfo.builder()
                                                                                      .build()))
                                                         .primaryKey("id")
                                                         .build()));
        return IntStreamEx.range(30)
                .mapToObj(i -> testCaseStep)
                .toList();
    }
}
