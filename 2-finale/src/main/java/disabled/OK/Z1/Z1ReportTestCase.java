package disabled.OK.Z1;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractReportTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import one.util.streamex.IntStreamEx;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.CreateReportRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.CreateReportRequestDto.ReportTable;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Order(41)
@Component
@RequiredArgsConstructor
public class Z1ReportTestCase extends AbstractReportTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        TestCaseStep testCaseStep = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Reports.CREATE_REPORT),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.createReport(
                        CreateReportRequestDto.builder()
                                .reportId((long) resultIdProvider.getNextUniqueId())
                                .tableAmount(2)
                                .tables(List.of(
                                        ReportTable.builder()
                                                .tableName("no_such_table_on_the_beach")
                                                .columns(List.of(ReportTable.ReportTableColumn.builder()
                                                                         .title("%")
                                                                         .build()))
                                                .build()))
                                .build()));
        return IntStreamEx.range(30)
                .mapToObj(i -> testCaseStep)
                .toList();
    }
}
