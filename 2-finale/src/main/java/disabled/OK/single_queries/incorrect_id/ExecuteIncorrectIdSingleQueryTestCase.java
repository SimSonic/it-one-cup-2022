package disabled.OK.single_queries.incorrect_id;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractSingleQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

// @Component
@RequiredArgsConstructor
@Slf4j
public class ExecuteIncorrectIdSingleQueryTestCase extends AbstractSingleQueryTestCase {

    private static final String INCORRECT_ID = "abcdefghijklmnopqrstuvwxyz";

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> remoteDbClient.executeSingleQuery(INCORRECT_ID))
        );
    }
}
