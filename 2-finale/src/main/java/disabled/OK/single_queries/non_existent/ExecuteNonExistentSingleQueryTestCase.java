package disabled.OK.single_queries.non_existent;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractSingleQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class ExecuteNonExistentSingleQueryTestCase extends AbstractSingleQueryTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> remoteDbClient.executeSingleQuery(String.valueOf(resultIdProvider.getNextUniqueId())))
        );
    }
}
