package disabled.OK.single_queries.non_existent;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractSingleQueryTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.ModifySingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class ModifyNonExistentSingleQueryTestCase extends AbstractSingleQueryTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.MODIFY_SINGLE_QUERY),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::modifyQueryById)
        );
    }

    private ResponseEntity<?> modifyQueryById() {
        ModifySingleQueryRequestDto requestDto = ModifySingleQueryRequestDto.builder()
                .queryId((long) resultIdProvider.getNextUniqueId())
                .query("KILL THEM ALL")
                .build();
        return remoteDbClient.modifySingleQuery(requestDto);
    }
}
