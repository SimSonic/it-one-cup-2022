package disabled.OK.tables;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.AddNewQueryToTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.utils.TestUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class GetDisappearedTableTestCase extends AbstractTableTestCase {

    private static final String OLOLO_TABLE_NAME = "Customer_Ололо";
    private static final String BUGAGA_TABLE_NAME = "Customer_БуГаГа";

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        long queryId = resultIdProvider.getNextUniqueId();

        return List.of(
                // Создание таблицы.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.CREATED,
                        this::createTable),

                // Запрос на переименование.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.CREATE_TABLE_QUERY),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.createTableQuery(
                                AddNewQueryToTableRequestDto.builder()
                                        .queryId(queryId)
                                        .query("ALTER TABLE %s RENAME TO %s".formatted(OLOLO_TABLE_NAME, BUGAGA_TABLE_NAME))
                                        .tableName(OLOLO_TABLE_NAME)
                                        .build())),

                // Выполнение запроса.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.executeTableQuery(String.valueOf(queryId))),

                // Получение таблицы по новому имени.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.GET_TABLE_BY_NAME),
                        HttpStatus.OK,
                        () -> remoteDbClient.getTable(BUGAGA_TABLE_NAME)),

                // Ну и удалим её тут же.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.dropTable(BUGAGA_TABLE_NAME))
        );
    }

    private ResponseEntity<?> createTable() {
        CreateTableRequestDto requestDto = TestUtils
                .getResource("/tests/tables/1-create-table-customer.json", CreateTableRequestDto.class)
                .withTableName(OLOLO_TABLE_NAME);
        return remoteDbClient.createTable(requestDto);
    }
}
