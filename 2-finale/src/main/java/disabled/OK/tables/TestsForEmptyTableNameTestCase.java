package disabled.OK.tables;

import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.utils.TestUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class TestsForEmptyTableNameTestCase extends AbstractTableTestCase {

    private static final CreateTableRequestDto ORIGINAL_CREATE_TABLE_REQUEST_DTO = TestUtils
            .getResource("/tests/tables/1-create-table-customer.json", CreateTableRequestDto.class);

    private static final String EMPTY_TABLE_NAME = StringUtils.EMPTY;
    private static final String BLANK_TABLE_NAME = StringUtils.SPACE;

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::createTableWithEmptyName),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::createTableWithBlankName),
                new TestCaseStepAdapter(
                        null,
                        HttpStatus.NOT_ACCEPTABLE,
                        this::getTableWithEmptyName),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::getTableWithBlankName),
                new TestCaseStepAdapter(
                        null,
                        HttpStatus.NOT_FOUND,
                        this::dropTableWithEmptyName),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::dropTableWithBlankName)
        );
    }

    private ResponseEntity<?> createTableWithEmptyName() {
        CreateTableRequestDto requestDto = ORIGINAL_CREATE_TABLE_REQUEST_DTO
                .withTableName(EMPTY_TABLE_NAME);
        return remoteDbClient.createTable(requestDto);
    }

    private ResponseEntity<?> createTableWithBlankName() {
        CreateTableRequestDto requestDto = ORIGINAL_CREATE_TABLE_REQUEST_DTO
                .withTableName(BLANK_TABLE_NAME);
        return remoteDbClient.createTable(requestDto);
    }

    private ResponseEntity<?> getTableWithEmptyName() {
        return remoteDbClient.getTable(EMPTY_TABLE_NAME);
    }

    private ResponseEntity<?> getTableWithBlankName() {
        return remoteDbClient.getTable(BLANK_TABLE_NAME);
    }

    private ResponseEntity<?> dropTableWithEmptyName() {
        return remoteDbClient.dropTable(EMPTY_TABLE_NAME);
    }

    private ResponseEntity<?> dropTableWithBlankName() {
        return remoteDbClient.dropTable(BLANK_TABLE_NAME);
    }
}
