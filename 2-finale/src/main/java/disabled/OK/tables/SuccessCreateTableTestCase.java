package disabled.OK.tables;

import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.utils.TestUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class SuccessCreateTableTestCase extends AbstractTableTestCase {

    private static final String CUSTOMER_TABLE_NAME = "Customer";

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.CREATED,
                        this::createTable),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.GET_TABLE_BY_NAME),
                        HttpStatus.OK,
                        this::getTable),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                        HttpStatus.CREATED,
                        this::deleteTable)
        );
    }

    private ResponseEntity<?> createTable() {
        String json = TestUtils.getResourceAsString("/tests/tables/1-create-table-customer.json");
        return remoteDbClient.createTable(json);
    }

    private ResponseEntity<?> getTable() {
        return remoteDbClient.getTable(CUSTOMER_TABLE_NAME);
    }

    private ResponseEntity<?> deleteTable() {
        return remoteDbClient.dropTable(CUSTOMER_TABLE_NAME);
    }
}
