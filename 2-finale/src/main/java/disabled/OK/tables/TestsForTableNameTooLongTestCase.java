package disabled.OK.tables;

import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.utils.TestUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class TestsForTableNameTooLongTestCase extends AbstractTableTestCase {

    /**
     * 51 символ.
     */
    private static final String LONG_TABLE_NAME = "CustomerCustomerCustomerCustomerCustomerCustomer123";

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::createTableWithTooLongName),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.GET_TABLE_BY_NAME),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::getTableWithTooLongName),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                        HttpStatus.NOT_ACCEPTABLE,
                        this::dropTableWithTooLongName)
        );
    }

    private ResponseEntity<?> createTableWithTooLongName() {
        CreateTableRequestDto requestDto = TestUtils
                .getResource("/tests/tables/1-create-table-customer.json", CreateTableRequestDto.class)
                .withTableName(LONG_TABLE_NAME);
        return remoteDbClient.createTable(requestDto);
    }

    private ResponseEntity<?> getTableWithTooLongName() {
        return remoteDbClient.getTable(LONG_TABLE_NAME);
    }

    private ResponseEntity<?> dropTableWithTooLongName() {
        return remoteDbClient.dropTable(LONG_TABLE_NAME);
    }
}
