package disabled.shutdown;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.AddNewQueryToTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.ModifyQueryInTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseGroup;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractShutdownTestCase;
import com.gitlab.simsonic.itone2022.server.utils.Queries;

import java.util.List;

@Order(1)
@Component
@RequiredArgsConstructor
@Slf4j
public class ShutdownUsingTableQueryFunctionAliasTestCase extends AbstractShutdownTestCase {

    private static final String TABLE_NAME = "xxx";

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        long queryId = resultIdProvider.getNextUniqueId();

        return List.of(
                // Создаём таблицу.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.createTable(
                                CreateTableRequestDto.builder()
                                        .tableName(TABLE_NAME)
                                        .columnsAmount(1)
                                        .columnInfos(List.of(CreateTableRequestDto.ColumnInfo.builder()
                                                                     .title("id")
                                                                     .type("INT")
                                                                     .build()))
                                        .primaryKey("id")
                                        .build()))
                        .inGroup(TestCaseGroup.TABLE_QUERY),

                // Создаём запрос v1.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.CREATE_TABLE_QUERY),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.createTableQuery(
                                AddNewQueryToTableRequestDto.builder()
                                        .tableName(TABLE_NAME)
                                        .queryId(queryId)
                                        .query(Queries.SHUTDOWN_FUNCTION_ALIAS_V1)
                                        .build()))
                        .inGroup(TestCaseGroup.TABLE_QUERY),
                // Запускаем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.executeTableQuery(String.valueOf(queryId)))
                        .inGroup(TestCaseGroup.TABLE_QUERY),

                // Модифицируем запрос на v2.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.MODIFY_TABLE_QUERY),
                        HttpStatus.OK,
                        () -> remoteDbClient.modifyTableQuery(
                                ModifyQueryInTableRequestDto.builder()
                                        .tableName(TABLE_NAME)
                                        .queryId(queryId)
                                        .query(Queries.SHUTDOWN_FUNCTION_ALIAS_V2)
                                        .build()))
                        .inGroup(TestCaseGroup.TABLE_QUERY),
                // Запускаем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.executeTableQuery(String.valueOf(queryId)))
                        .inGroup(TestCaseGroup.TABLE_QUERY)
        );
    }
}
