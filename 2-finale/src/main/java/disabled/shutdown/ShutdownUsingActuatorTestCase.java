package disabled.shutdown;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;

import com.gitlab.simsonic.itone2022.server.testcases.TestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;

import java.util.List;

@Order(101)
// @Component
@RequiredArgsConstructor
@Slf4j
public class ShutdownUsingActuatorTestCase implements TestCase {

    private final RemoteDbClient remoteDbClient;

    @Override
    public List<TestCaseStep> getSteps() {
        return List.of(
                new TestCaseStepAdapter(
                        null,
                        HttpStatus.NOT_FOUND,
                        remoteDbClient::shutdownUsingActuator)
        );
    }
}
