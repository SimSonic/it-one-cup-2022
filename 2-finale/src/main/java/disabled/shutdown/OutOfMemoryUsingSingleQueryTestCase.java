package disabled.shutdown;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.AddNewSingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractShutdownTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

// @Component
@RequiredArgsConstructor
@Slf4j
public class OutOfMemoryUsingSingleQueryTestCase extends AbstractShutdownTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        Context context = new Context(
                resultIdProvider.getNextUniqueId(),
                resultIdProvider.getNextUniqueId(),
                resultIdProvider.getNextUniqueId(),
                resultIdProvider.getNextUniqueId()
        );

        return List.of(
                // Создаём таблицу.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                        HttpStatus.CREATED,
                        () -> {
                            AddNewSingleQueryRequestDto requestDto = AddNewSingleQueryRequestDto.builder()
                                    .queryId(context.getCreateTableQueryId())
                                    .query("CREATE TABLE a (i INT PRIMARY KEY, t VARCHAR)")
                                    .build();
                            return remoteDbClient.createSingleQuery(requestDto);
                        }),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> executeQueryById(context.getCreateTableQueryId())),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.DELETE_SINGLE_QUERY_BY_ID),
                        HttpStatus.ACCEPTED,
                        () -> dropQueryById(context.getCreateTableQueryId())),

                // Вставляем в неё начальные данные.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                        HttpStatus.CREATED,
                        () -> {
                            AddNewSingleQueryRequestDto requestDto = AddNewSingleQueryRequestDto.builder()
                                    .queryId(context.getInsertIntoTableQueryId())
                                    .query("INSERT INTO a(i) VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20)")
                                    .build();
                            return remoteDbClient.createSingleQuery(requestDto);
                        }),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> executeQueryById(context.getInsertIntoTableQueryId())),

                // Многократно увеличиваем число строк — 86M строк.
                // P.S. Добавил ещё 3 JOIN-а, руиним по полной.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                        HttpStatus.CREATED,
                        () -> {
                            AddNewSingleQueryRequestDto requestDto = AddNewSingleQueryRequestDto.builder()
                                    .queryId(context.getMultipleRowsQueryId())
                                    .query("""
                                                   INSERT INTO a(i)
                                                   SELECT ROWNUM+x.m
                                                   FROM a
                                                   JOIN(SELECT MAX(i) FROM a)x(m)
                                                   JOIN a JOIN a JOIN a JOIN a JOIN a JOIN a
                                                   """)
                                    .build();
                            return remoteDbClient.createSingleQuery(requestDto);
                        }),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> executeQueryById(context.getMultipleRowsQueryId())),

                // Ещё и установим текстовую строку.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                        HttpStatus.CREATED,
                        () -> {
                            AddNewSingleQueryRequestDto requestDto = AddNewSingleQueryRequestDto.builder()
                                    .queryId(context.getSetTextQueryId())
                                    .query("UPDATE a SET t=CONCAT(i,'__abcdefghijklmnopqrstuvwxyz_abcdefghijklmnopqrstuvwxyz_abcdefghijklmnopqrstuvwxyz_0123456789')")
                                    .build();
                            return remoteDbClient.createSingleQuery(requestDto);
                        }),
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> executeQueryById(context.getSetTextQueryId()))
        );
    }

    private ResponseEntity<?> executeQueryById(long queryId) {
        return remoteDbClient.executeSingleQuery(String.valueOf(queryId));
    }

    private ResponseEntity<?> dropQueryById(long queryId) {
        return remoteDbClient.deleteSingleQuery(String.valueOf(queryId));
    }

    @Data
    private static class Context {

        private final long createTableQueryId;
        private final long insertIntoTableQueryId;
        private final long multipleRowsQueryId;
        private final long setTextQueryId;
    }
}
