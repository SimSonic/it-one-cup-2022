package disabled.shutdown;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.AddNewSingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractShutdownTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

// @Component
@RequiredArgsConstructor
@Slf4j
public class ShutdownUsingSingleQueryTestCase extends AbstractShutdownTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        Context context = new Context(
                resultIdProvider.getNextUniqueId()
        );

        return List.of(
                // Создаём запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                        HttpStatus.CREATED,
                        () -> createSingleQuery(context)),
                // Запускаем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> executeQueryById(context.getShutdownQueryId()))
        );
    }

    private ResponseEntity<?> createSingleQuery(Context context) {
        AddNewSingleQueryRequestDto requestDto = AddNewSingleQueryRequestDto.builder()
                .queryId(context.getShutdownQueryId())
                .query("SHUTDOWN IMMEDIATELY")
                .build();
        return remoteDbClient.createSingleQuery(requestDto);
    }

    private ResponseEntity<?> executeQueryById(long queryId) {
        return remoteDbClient.executeSingleQuery(String.valueOf(queryId));
    }

    @Data
    private static class Context {

        private final long shutdownQueryId;
    }
}
