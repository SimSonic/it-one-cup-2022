package disabled.shutdown;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.AddNewQueryToTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.ModifyQueryInTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractShutdownTestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;

import java.util.List;

// @Component
@RequiredArgsConstructor
@Slf4j
public class ShutdownUsingTableQueryTestCase extends AbstractShutdownTestCase {

    private static final String TABLE_NAME = "PISH_pish_ololo_1234567_abcdefghijklmnopqrstuvwxyz";

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        Context context = new Context(
                resultIdProvider.getNextUniqueId()
        );

        return List.of(
                // Создаём таблицу.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                        HttpStatus.CREATED,
                        () -> createTable(context)),

                // Добавляем запрос на выключение.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.CREATE_TABLE_QUERY),
                        HttpStatus.CREATED,
                        () -> createTableQuery(context.getShutdownQueryId())),

                // Запускаем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> executeQueryById(context.getShutdownQueryId())),

                // Изменяем текст запроса на смайлики.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.MODIFY_TABLE_QUERY),
                        HttpStatus.CREATED,
                        () -> modifyTableQuery(context.getShutdownQueryId())),

                // Запускаем некорректный запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                        HttpStatus.NOT_ACCEPTABLE,
                        () -> executeQueryById(context.getShutdownQueryId())),

                // Удаляем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.DELETE_TABLE_QUERY_BY_ID),
                        HttpStatus.OK,
                        () -> dropQuery(context.getShutdownQueryId())),

                // Удаляем созданную таблицу.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                        HttpStatus.ACCEPTED,
                        this::dropTable)
        );
    }

    private ResponseEntity<?> createTable(Context context) {
        CreateTableRequestDto requestDto = CreateTableRequestDto.builder()
                .tableName(TABLE_NAME)
                .columnsAmount(1)
                .columnInfos(List.of(CreateTableRequestDto.ColumnInfo.builder()
                                             .title("id")
                                             .type("BIGINT")
                                             .build()))
                .primaryKey("id")
                .build();
        return remoteDbClient.createTable(requestDto);
    }

    private ResponseEntity<?> createTableQuery(long queryId) {
        AddNewQueryToTableRequestDto requestDto = AddNewQueryToTableRequestDto.builder()
                .tableName(TABLE_NAME)
                .queryId(queryId)
                .query("SHUTDOWN IMMEDIATELY")
                .build();
        return remoteDbClient.createTableQuery(requestDto);
    }

    private ResponseEntity<?> modifyTableQuery(long queryId) {
        ModifyQueryInTableRequestDto requestDto = ModifyQueryInTableRequestDto.builder()
                .tableName(TABLE_NAME)
                .queryId(queryId)
                .query("💃🏝️👀🤣🎻🛢️☎️🖍️⏲️🥩👩🏽‍❤️‍👩🏽🏫🔱🛺♥️🧛‍♀️🎇")
                .build();
        return remoteDbClient.modifyTableQuery(requestDto);
    }

    private ResponseEntity<?> executeQueryById(long queryId) {
        return remoteDbClient.executeTableQuery(String.valueOf(queryId));
    }

    private ResponseEntity<?> dropQuery(long queryId) {
        return remoteDbClient.deleteSingleQuery(String.valueOf(queryId));
    }

    private ResponseEntity<?> dropTable() {
        return remoteDbClient.dropTable(TABLE_NAME);
    }

    @Data
    private static class Context {

        private final long shutdownQueryId;
    }
}
