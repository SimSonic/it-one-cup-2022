package disabled.shutdown;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.AddNewSingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.ModifySingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseGroup;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractShutdownTestCase;
import com.gitlab.simsonic.itone2022.server.utils.Queries;

import java.util.List;

@Order(2)
@Component
@RequiredArgsConstructor
@Slf4j
public class ShutdownUsingSingleQueryFunctionAliasTestCase extends AbstractShutdownTestCase {

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    @Override
    public List<TestCaseStep> getSteps() {
        long queryId = resultIdProvider.getNextUniqueId();

        return List.of(
                // Создаём запрос v1.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.createSingleQuery(
                                AddNewSingleQueryRequestDto.builder()
                                        .queryId(queryId)
                                        .query(Queries.SHUTDOWN_FUNCTION_ALIAS_V1)
                                        .build()))
                        .inGroup(TestCaseGroup.SINGLE_QUERY),

                // Запускаем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.executeSingleQuery(String.valueOf(queryId)))
                        .inGroup(TestCaseGroup.SINGLE_QUERY),

                // Модифицируем запрос на v2.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.MODIFY_SINGLE_QUERY),
                        HttpStatus.OK,
                        () -> remoteDbClient.modifySingleQuery(
                                ModifySingleQueryRequestDto.builder()
                                        .queryId(queryId)
                                        .query(Queries.SHUTDOWN_FUNCTION_ALIAS_V2)
                                        .build()))
                        .inGroup(TestCaseGroup.SINGLE_QUERY),
                // Запускаем запрос.
                new TestCaseStepAdapter(
                        RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                        HttpStatus.CREATED,
                        () -> remoteDbClient.executeSingleQuery(String.valueOf(queryId)))
                        .inGroup(TestCaseGroup.SINGLE_QUERY)
        );
    }
}
