package com.gitlab.simsonic.itone2022.server.remotedbclient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.CreateReportRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.AddNewSingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.ModifySingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.SingleQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.AddNewQueryToTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.ModifyQueryInTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.GetTableResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.http.RestApiHttpClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class RemoteDbClient {

    private final RestApiHttpClient restApiHttpClient;
    private final ResultIdProvider resultIdProvider;

    public ResponseEntity<String> registerTestCaseStepResult(String url, HttpStatus status, String field, Object value) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("resultId", resultIdProvider.getCurrentResultId());
        payload.put("code", status.value());

        if (StringUtils.isNotBlank(field) && value != null) {
            payload.put(field, value);
        }

        return resultIdProvider.executeRegistration(() -> restApiHttpClient.postForEntity(
                payload,
                url,
                RemoteDbTypes.STRING_TYPE));
    }

    public ResponseEntity<String> createTable(CreateTableRequestDto requestDto) {
        return restApiHttpClient.postForEntity(
                requestDto,
                RemoteDbUrls.Tables.CREATE_TABLE,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> createTable(String requestJson) {
        return restApiHttpClient.postForEntity(
                requestJson,
                RemoteDbUrls.Tables.CREATE_TABLE,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<GetTableResponseDto> getTable(String tableName) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.Tables.GET_TABLE_BY_NAME + tableName,
                RemoteDbTypes.Tables.GET_TABLE_BY_NAME_RESPONSE);
    }

    public ResponseEntity<String> dropTable(String tableName) {
        return restApiHttpClient.deleteForEntity(
                null,
                RemoteDbUrls.Tables.DROP_TABLE + tableName,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> createTableQuery(AddNewQueryToTableRequestDto requestDto) {
        return restApiHttpClient.postForEntity(
                requestDto,
                RemoteDbUrls.TableQueries.CREATE_TABLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> createTableQuery(String requestJson) {
        return restApiHttpClient.postForEntity(
                requestJson,
                RemoteDbUrls.TableQueries.CREATE_TABLE_QUERY,
                RemoteDbTypes.STRING_TYPE);

    }

    public ResponseEntity<String> modifyTableQuery(ModifyQueryInTableRequestDto requestDto) {
        return restApiHttpClient.putForEntity(
                requestDto,
                RemoteDbUrls.TableQueries.MODIFY_TABLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> modifyTableQuery(String requestJson) {
        return restApiHttpClient.putForEntity(
                requestJson,
                RemoteDbUrls.TableQueries.MODIFY_TABLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> deleteTableQuery(String queryId) {
        return restApiHttpClient.deleteForEntity(
                null,
                RemoteDbUrls.TableQueries.DELETE_TABLE_QUERY_BY_ID + queryId,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> executeTableQuery(String queryId) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID + queryId,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<TableQueryResponseDto> getTableQuery(String queryId) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.TableQueries.GET_TABLE_QUERY_BY_ID + queryId,
                RemoteDbTypes.TableQueries.TABLE_QUERY_RESPONSE);
    }

    public ResponseEntity<List<TableQueryResponseDto>> getTableQueries(String tableName) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.TableQueries.GET_TABLE_QUERIES_BY_TABLE_NAME + tableName,
                RemoteDbTypes.TableQueries.TABLE_QUERIES_LIST_RESPONSE);
    }

    public ResponseEntity<List<TableQueryResponseDto>> getAllTableQueries() {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.TableQueries.GET_ALL_TABLE_QUERIES,
                RemoteDbTypes.TableQueries.TABLE_QUERIES_LIST_RESPONSE);
    }

    public ResponseEntity<String> createSingleQuery(AddNewSingleQueryRequestDto requestDto) {
        return restApiHttpClient.postForEntity(
                requestDto,
                RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> createSingleQuery(String requestJson) {
        return restApiHttpClient.postForEntity(
                requestJson,
                RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> modifySingleQuery(ModifySingleQueryRequestDto requestDto) {
        return restApiHttpClient.putForEntity(
                requestDto,
                RemoteDbUrls.SingleQueries.MODIFY_SINGLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> modifySingleQuery(String requestJson) {
        return restApiHttpClient.putForEntity(
                requestJson,
                RemoteDbUrls.SingleQueries.MODIFY_SINGLE_QUERY,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> deleteSingleQuery(String queryId) {
        return restApiHttpClient.deleteForEntity(
                null,
                RemoteDbUrls.SingleQueries.DELETE_SINGLE_QUERY_BY_ID + queryId,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<String> executeSingleQuery(String queryId) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID + queryId,
                RemoteDbTypes.STRING_TYPE);
    }

    public ResponseEntity<SingleQueryResponseDto> getSingleQuery(String queryId) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.SingleQueries.GET_SINGLE_QUERY_BY_ID + queryId,
                RemoteDbTypes.SingleQueries.SINGLE_QUERY_RESPONSE);
    }

    public ResponseEntity<List<SingleQueryResponseDto>> getAllSingleQueries() {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.SingleQueries.GET_ALL_SINGLE_QUERIES,
                RemoteDbTypes.SingleQueries.SINGLE_QUERIES_LIST_RESPONSE);
    }

    public ResponseEntity<Void> createReport(CreateReportRequestDto requestDto) {
        return restApiHttpClient.postForEntity(
                requestDto,
                RemoteDbUrls.Reports.CREATE_REPORT,
                RemoteDbTypes.VOID_TYPE);
    }

    public ResponseEntity<Void> createReport(String requestJson) {
        return restApiHttpClient.postForEntity(
                requestJson,
                RemoteDbUrls.Reports.CREATE_REPORT,
                RemoteDbTypes.VOID_TYPE);
    }

    public ResponseEntity<GetReportDataResponseDto> getReport(String reportId) {
        return restApiHttpClient.getForEntity(
                RemoteDbUrls.Reports.GET_REPORT_BY_ID + reportId,
                RemoteDbTypes.Reports.GET_REPORT_RESPONSE_TYPE);
    }

    public ResponseEntity<Void> shutdownUsingActuator() {
        return restApiHttpClient.postForEntity(
                null,
                "/actuator/shutdown",
                RemoteDbTypes.VOID_TYPE);
    }
}
