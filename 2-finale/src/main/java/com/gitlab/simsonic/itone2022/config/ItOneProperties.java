package com.gitlab.simsonic.itone2022.config;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties("it-one")
@ConstructorBinding
@Value
@Validated
public class ItOneProperties {

    String something;
}
