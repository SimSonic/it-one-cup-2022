package com.gitlab.simsonic.itone2022.server.testcases.adapters;

import com.gitlab.simsonic.itone2022.server.testcases.TestCase;
import org.springframework.core.annotation.Order;

@Order(100)
public abstract class AbstractShutdownTestCase implements TestCase {

}
