package com.gitlab.simsonic.itone2022.server.checker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.testcases.TestCase;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseGroup;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
@Slf4j
public class CheckerService implements CommandLineRunner {

    private final List<TestCase> testCases;
    private final ResultIdProvider resultIdProvider;
    private final RemoteDbClient remoteDbClient;

    private final AtomicInteger enemyIdContainer = new AtomicInteger();

    private final Map<Integer, Integer> statisticsByEnemyId = new ConcurrentHashMap<>();

    @SuppressWarnings("BooleanVariableAlwaysNegated")
    @Value("${fail-fast:true}")
    private final boolean failFastOnRegistration404;

    @Override
    public void run(String... args) {
        log.debug("Registered {} test cases.", testCases.size());
    }

    public void runAllTestCases() {
        StopWatch stopWatch = StopWatch.createStarted();
        int enemyId = enemyIdContainer.incrementAndGet();
        log.info("🎉 Incoming '/api/start' request # {}!", enemyId);

        try {
            statisticsByEnemyId.put(enemyId, 0);

            for (TestCase testCase : testCases) {
                runTestCase(testCase, enemyId);
            }

            log.info("Tests finished for enemy {} with {} failures in {}.", enemyId, statisticsByEnemyId.get(enemyId), stopWatch);
        } catch (Exception ex) {
            log.error("Tests stopped for enemy {} with: {}", enemyId, ex.getMessage());
        } finally {
            statisticsByEnemyId.remove(enemyId);
        }
    }

    private void runTestCase(TestCase testCase, int enemyId) {
        log.debug("✨ {}", testCase.getClass().getSimpleName());

        Map<TestCaseGroup, Integer> requestsByGroup = new EnumMap<>(TestCaseGroup.class);
        Arrays.stream(TestCaseGroup.values()).forEach(tcg -> requestsByGroup.put(tcg, 0));

        List<TestCaseStep> steps = testCase.getSteps();

        for (TestCaseStep step : steps) {
            boolean repeatSuccessfulStep;
            do {
                repeatSuccessfulStep = false;
                // Если пришёл новый внешний запрос, текущий заканчиваем.
                boolean interrupt = enemyId != enemyIdContainer.get();
                if (interrupt) {
                    log.warn("Interrupting enemyId = {}", enemyId);
                    break;
                }

                // Получаем новый ID ожидаемого ответа и всё делаем в его рамках.
                TestStepResult result = resultIdProvider.executeWithinNewResultId(() -> executeTestCaseStep(step, enemyId));
                TestCaseGroup testCaseGroup = step.getTestCaseGroup();
                if (testCaseGroup != null) {
                    int currentValue = requestsByGroup.compute(
                            testCaseGroup,
                            (k, v) -> 1 + ObjectUtils.defaultIfNull(v, 0));
                    int testCaseGroupLimit = testCaseGroup.getLimit();
                    if (currentValue < testCaseGroupLimit) {
                        // Может повторим?
                        repeatSuccessfulStep = true;
                        // repeatSuccessfulStep = ThreadLocalRandom.current().nextBoolean();
                        // repeatSuccessfulStep = false;
                    }
                }

                // Если шаг завершился ошибкой, прекращаем выполнение тест-кейса.
                switch (result) {
                    // Мы выполнили что-то незаконное, не смогли пройти ЭТАЛОН.
                    // Или квалификационное решение совсем отвалилось.
                    case CANNOT_REGISTER, MY_FAULT, DISCONNECTED -> {
                        return;
                    }
                    // Не будем пытаться это повторить, нет смысла.
                    case VALID -> repeatSuccessfulStep = false;
                    // Ура, мы завалили конкурента! Можно ещё немного его повалить.
                    case RUINED -> {
                    }
                }
            } while (repeatSuccessfulStep);
        }
    }

    private TestStepResult executeTestCaseStep(TestCaseStep step, int enemyId) {
        HttpStatus expectedResponseStatus = step.getExpectedResponseStatus();
        Object expectedResponse = step.getExpectedResponse();

        // Регистрируем новый ожидаемый ответ.
        if (!isResultRegistered(step, expectedResponseStatus, expectedResponse)) {
            return TestStepResult.CANNOT_REGISTER;
        }

        try {
            // Выполняем запрос к тестируемой системе через прокси.
            ResponseEntity<?> actualResponseEntity = step.execute();
            HttpStatus actualResponseStatus = actualResponseEntity.getStatusCode();
            if (actualResponseStatus == HttpStatus.BAD_REQUEST
                || actualResponseStatus == HttpStatus.FORBIDDEN
                || actualResponseStatus == HttpStatus.NOT_FOUND) {
                // Мы выполнили какой-то некорректный запрос.
                log.warn("🧧 Response status is unexpected: {} (expected {})",
                         actualResponseStatus.value(),
                         expectedResponseStatus.value());
                return TestStepResult.MY_FAULT;
            }

            if (actualResponseStatus != expectedResponseStatus) {
                // Потенциально сломали здесь противника (1).
                log.warn("👍 Response status is unexpected: {} (expected {})",
                         actualResponseStatus.value(),
                         expectedResponseStatus.value());
                statisticsByEnemyId.compute(enemyId, (k, v) -> 1 + ObjectUtils.defaultIfNull(v, 0));
                return TestStepResult.RUINED;
            }

            Object actualResponse = actualResponseEntity.getBody();
            if (expectedResponse != null && !expectedResponse.equals(actualResponse)) {
                // Потенциально сломали здесь противника (2).
                log.warn("👍 Response body is unexpected: {}",
                         actualResponse);
                statisticsByEnemyId.compute(enemyId, (k, v) -> 1 + ObjectUtils.defaultIfNull(v, 0));
                return TestStepResult.RUINED;
            }

            return TestStepResult.VALID;
        } catch (ResourceAccessException ex) {
            return TestStepResult.DISCONNECTED;
        }
    }

    private boolean isResultRegistered(TestCaseStep step, HttpStatus expectedResponseStatus, Object expectedResponse) {
        String resultRegistrationUrl = step.getResultRegistrationUrl();
        if (StringUtils.isBlank(resultRegistrationUrl)) {
            return true;
        }

        ResponseEntity<String> responseEntity = remoteDbClient.registerTestCaseStepResult(
                resultRegistrationUrl,
                expectedResponseStatus,
                step.getExpectedResponseFieldName(),
                expectedResponse);

        // Не смогли зарегистрировать ожидаемый результат.
        if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            log.warn("Expected result cannot be registered: {}\n{}",
                     responseEntity.getStatusCode(),
                     responseEntity.getBody());
            return !failFastOnRegistration404;
        }

        return true;
    }

    enum TestStepResult {

        VALID,

        RUINED,

        MY_FAULT,

        DISCONNECTED,

        CANNOT_REGISTER,
    }
}
