package com.gitlab.simsonic.itone2022.server.common.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import com.gitlab.simsonic.itone2022.server.common.exceptions.base.CommonExceptionDescription;
import com.gitlab.simsonic.itone2022.server.common.exceptions.base.LogType;

/**
 * Типы исключительных ситуаций, которые могут возникать в приложении.
 */
@RequiredArgsConstructor
@Getter
public enum CommonExceptions implements CommonExceptionDescription {

    NOT_FOUND(LogType.SKIP,
              HttpStatus.NOT_FOUND,
              "Некорректный URI запроса"),

    UNSUPPORTED(
            LogType.NOTE,
            HttpStatus.BAD_REQUEST,
            "Функционал не поддерживается"),

    VALIDATION_FAILED(
            LogType.NOTE,
            HttpStatus.NOT_ACCEPTABLE,
            "Ошибка валидации"),

    NOT_IMPLEMENTED_YET(
            LogType.NOTE,
            HttpStatus.NOT_IMPLEMENTED,
            "Функционал ещё не реализован"),

    INTERNAL_SERVER_WARNING(LogType.SKIP,
                            HttpStatus.I_AM_A_TEAPOT,
                            "Возникла неожиданная, но некритичная ситуация"),

    INTERNAL_SERVER_ERROR(LogType.ERROR,
                          HttpStatus.INTERNAL_SERVER_ERROR,
                          "Внутренняя ошибка сервера"),

    UNAUTHORIZED(LogType.SKIP,
                 HttpStatus.UNAUTHORIZED,
                 "Нет данных текущего пользователя"),
    ;

    /**
     * Каким образом логировать ошибку.
     */
    private final LogType logType;

    /**
     * Какой HTTP статус вернуть клиенту.
     */
    private final HttpStatus httpStatus;

    /**
     * Сообщение по умолчанию.
     * Является префиксом в случае, если в исключении указано собственное сообщение.
     */
    private final String defaultMessage;

    /**
     * Код-название.
     */
    @Override
    public String getName() {
        return name();
    }
}
