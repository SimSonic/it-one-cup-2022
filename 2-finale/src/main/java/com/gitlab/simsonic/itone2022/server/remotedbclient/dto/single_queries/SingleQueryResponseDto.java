package com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SingleQueryResponseDto {

    long queryId;
    String query;
}
