package com.gitlab.simsonic.itone2022.server.common;

import lombok.Builder;
import lombok.Value;

/**
 * Описание произошедшей ошибки.
 */
@Value
@Builder
public class ErrorResponseDto {

    /**
     * Доказываем МП или UI, что это ошибка.
     * Всегда равно TRUE.
     */
    @SuppressWarnings("FieldMayBeStatic")
    boolean error = true;

    /**
     * Текстовое описание ситуации.
     */
    String errorMessage;

    /**
     * Машино-читаемый код ошибки.
     */
    String errorCode;
}
