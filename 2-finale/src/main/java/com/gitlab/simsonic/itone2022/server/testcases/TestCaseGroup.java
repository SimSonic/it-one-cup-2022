package com.gitlab.simsonic.itone2022.server.testcases;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum TestCaseGroup {

    TABLE(30),
    TABLE_QUERY(20),
    SINGLE_QUERY(20),
    REPORT(30),
    ;

    private final int limit;
}
