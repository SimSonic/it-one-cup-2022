package com.gitlab.simsonic.itone2022.server.remotedbclient.http;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.gitlab.simsonic.itone2022.config.ProxyEndpointProvider;
import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.common.ErrorResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbTypes;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Реализация GET, POST и остальных вызовов к контроллерам приложения.
 */
@Component
@RequiredArgsConstructor
@Getter
@Slf4j
class RestApiHttpHelper {

    private static final ParameterizedTypeReference<ErrorResponseDto> ERROR_RESPONSE_TYPE = ParameterizedTypeReference
            .forType(ErrorResponseDto.class);

    private final ProxyEndpointProvider proxyEndpointProvider;
    private final ResultIdProvider resultIdProvider;

    private final HttpExchangeable httpExchangeable;

    @NonNull
    public <DTO> DTO get(String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = getExchange(method, responseType);
        return response.getBody();
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> getWithError(String method) {
        ResponseEntity<ErrorResponseDto> response = getExchange(method, ERROR_RESPONSE_TYPE);
        return response;
    }

    @NonNull
    public <DTO> DTO post(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = postExchange(requestDto, method, responseType);
        return response.getBody();
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> postWithError(Object requestDto, String method) {
        ResponseEntity<ErrorResponseDto> response = postExchange(requestDto, method, RemoteDbTypes.ERROR_RESPONSE_TYPE);
        return response;
    }

    @NonNull
    public <DTO> DTO put(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = putExchange(requestDto, method, responseType);
        return response.getBody();
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> putWithError(Object requestDto, String method) {
        ResponseEntity<ErrorResponseDto> response = putExchange(requestDto, method, RemoteDbTypes.ERROR_RESPONSE_TYPE);
        return response;
    }

    @NonNull
    public <DTO> DTO delete(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = deleteExchange(requestDto, method, responseType);
        return response.getBody();
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> deleteWithError(Object requestDto, String method) {
        return deleteExchange(requestDto, method, RemoteDbTypes.ERROR_RESPONSE_TYPE);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> getExchange(String method, ParameterizedTypeReference<DTO> responseType) {
        return exchange(HttpMethod.GET, method, null, responseType);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> postExchange(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        return exchange(HttpMethod.POST, method, requestDto, responseType);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> putExchange(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        return exchange(HttpMethod.PUT, method, requestDto, responseType);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> deleteExchange(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        return exchange(HttpMethod.DELETE, method, requestDto, responseType);
    }

    @NonNull
    private <DTO> ResponseEntity<DTO> exchange(
            HttpMethod httpMethod,
            String methodUri,
            Object request,
            ParameterizedTypeReference<DTO> responseType
    ) {
        String proxyEndpoint = proxyEndpointProvider.getProxyEndpoint();

        Optional<Integer> resultId = Optional
                .ofNullable(resultIdProvider.getCurrentResultId())
                .filter(rid -> !resultIdProvider.isInsideRegistration());

        URI uri = UriComponentsBuilder.fromHttpUrl(proxyEndpoint + methodUri)
                .queryParamIfPresent("resultId", resultId)
                .build(Map.of());

        HttpHeaders httpHeaders = new HttpHeaders();
        boolean hasRequestPayload = request != null;
        if (hasRequestPayload) {
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        }

        var requestEntity = new RequestEntity<>(request, httpHeaders, httpMethod, uri);
        var responseEntity = httpExchangeable.exchange(requestEntity, responseType);

        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.is2xxSuccessful()) {
            log.debug("==> ✔ {} {}", httpMethod, uri);
            if (hasRequestPayload) {
                // log.debug("==> SENT BODY: {}", request);
            }
        } else {
            log.debug("==> ❌ {} {}\n{}\nPAYLOAD: {}", httpMethod, uri, statusCode, responseEntity.getBody());
            if (hasRequestPayload) {
                // log.debug("==> SENT BODY: {}", request);
            }
        }

        return responseEntity;
    }
}
