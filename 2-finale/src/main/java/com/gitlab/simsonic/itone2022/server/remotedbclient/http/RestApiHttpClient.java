package com.gitlab.simsonic.itone2022.server.remotedbclient.http;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.server.common.ErrorResponseDto;

import java.util.concurrent.Callable;

/**
 * Реализация тестовых бизнес-вызовов к контроллерам приложения.
 */
@SuppressWarnings({"unused", "UnusedReturnValue"})
@Component
@RequiredArgsConstructor
@Slf4j
public class RestApiHttpClient {

    private final RestApiHttpHelper restApiHttpHelper;

    private static long totalRequestTime;

    public void reset() {
        log.info("Total API request time: {} ms.", totalRequestTime);
    }

    @NonNull
    public <DTO> DTO get(String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.get(url, responseType));
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> getForEntity(String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.getExchange(url, responseType));
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> getWithError(String url) {
        return measure(() -> restApiHttpHelper.getWithError(url));
    }

    @NonNull
    public <DTO> DTO post(Object requestDto, String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.post(requestDto, url, responseType));
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> postForEntity(Object requestDto, String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.postExchange(requestDto, url, responseType));
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> postWithError(Object requestDto, String url) {
        return measure(() -> restApiHttpHelper.postWithError(requestDto, url));
    }

    @NonNull
    public <DTO> DTO put(Object requestDto, String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.put(requestDto, url, responseType));
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> putForEntity(Object requestDto, String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.putExchange(requestDto, url, responseType));
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> putWithError(Object requestDto, String url) {
        return measure(() -> restApiHttpHelper.putWithError(requestDto, url));
    }

    @NonNull
    public <DTO> DTO delete(Object requestDto, String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.delete(requestDto, url, responseType));
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> deleteForEntity(Object requestDto, String url, ParameterizedTypeReference<DTO> responseType) {
        return measure(() -> restApiHttpHelper.deleteExchange(requestDto, url, responseType));
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> deleteWithError(Object requestDto, String url) {
        return measure(() -> restApiHttpHelper.deleteWithError(requestDto, url));
    }

    @SneakyThrows
    private static <T> T measure(Callable<T> callable) {
        long t0 = System.currentTimeMillis();
        try {
            return callable.call();
        } finally {
            totalRequestTime += System.currentTimeMillis() - t0;
        }
    }
}
