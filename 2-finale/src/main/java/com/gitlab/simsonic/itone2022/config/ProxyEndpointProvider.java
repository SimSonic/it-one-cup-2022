package com.gitlab.simsonic.itone2022.config;

public interface ProxyEndpointProvider {

    String getProxyEndpoint();
}
