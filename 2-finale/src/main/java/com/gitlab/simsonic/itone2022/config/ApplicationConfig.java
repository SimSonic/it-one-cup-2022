package com.gitlab.simsonic.itone2022.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import com.gitlab.simsonic.itone2022.server.remotedbclient.http.HttpExchangeable;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@Slf4j
class ApplicationConfig {

    private static final int TASK_EXECUTOR_CORE_POOL_SIZE = 3;
    private static final int TASK_EXECUTOR_MAXIMUM_POOL_SIZE = TASK_EXECUTOR_CORE_POOL_SIZE;
    private static final int TASK_EXECUTOR_QUEUE_CAPACITY = 1024;
    private static final int TASK_EXECUTOR_THREAD_TTL = 30;

    @Autowired
    void setupObjectMapperBuilder(Jackson2ObjectMapperBuilder builder) {
        builder
                .findModulesViaServiceLoader(true)
                .featuresToDisable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Bean
    HttpExchangeable httpExchangeable(
            @Qualifier("restTemplateNoOp") RestTemplate restTemplate
    ) {
        return restTemplate::exchange;
    }

    @Bean
    ProxyEndpointProvider proxyEndpointProvider(
            @Value("${rs.endpoint}") String remoteServiceEndpoint
    ) {
        return () -> remoteServiceEndpoint;
    }

    @Bean
    ResultIdProvider resultIdProvider() {
        return new AtomicResultIdProvider();
    }

    @Bean
    @Qualifier("taskExecutor")
    ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setThreadNamePrefix("A-");

        taskExecutor.setCorePoolSize(TASK_EXECUTOR_CORE_POOL_SIZE);
        taskExecutor.setMaxPoolSize(TASK_EXECUTOR_MAXIMUM_POOL_SIZE);
        taskExecutor.setQueueCapacity(TASK_EXECUTOR_QUEUE_CAPACITY);
        taskExecutor.setKeepAliveSeconds(TASK_EXECUTOR_THREAD_TTL);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);

        log.debug("Parallelism set to {}", TASK_EXECUTOR_CORE_POOL_SIZE);
        return taskExecutor;
    }

    private static class AtomicResultIdProvider implements ResultIdProvider {

        private static final AtomicInteger RESULT_ID_GENERATOR = new AtomicInteger();
        private static final ThreadLocal<Integer> RESULT_ID_THREAD_LOCAL = new ThreadLocal<>();
        private static final ThreadLocal<Boolean> INSIDE_REGISTRATION = new ThreadLocal<>();

        @Override
        public int getNextUniqueId() {
            return RESULT_ID_GENERATOR.incrementAndGet();
        }

        @SneakyThrows
        @Override
        public <V> V executeWithinNewResultId(Callable<V> callable) {
            try {
                int resultId = getNextUniqueId();
                RESULT_ID_THREAD_LOCAL.set(resultId);
                return callable.call();
            } finally {
                RESULT_ID_THREAD_LOCAL.remove();
            }
        }

        @Override
        public Integer getCurrentResultId() {
            return RESULT_ID_THREAD_LOCAL.get();
        }

        @SneakyThrows
        @Override
        public <V> V executeRegistration(Callable<V> callable) {
            try {
                INSIDE_REGISTRATION.set(true);
                return callable.call();
            } finally {
                INSIDE_REGISTRATION.remove();
            }
        }

        @Override
        public boolean isInsideRegistration() {
            return BooleanUtils.isTrue(INSIDE_REGISTRATION.get());
        }
    }
}
