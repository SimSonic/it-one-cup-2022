package com.gitlab.simsonic.itone2022.server.testcases;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;

import java.util.function.Supplier;

@RequiredArgsConstructor
@Getter
public class TestCaseStepAdapter implements TestCaseStep {

    private final String resultRegistrationUrl;
    private final HttpStatus expectedResponseStatus;
    private final String expectedResponseFieldName;
    private final Object expectedResponse;
    private final Supplier<ResponseEntity<?>> requestExecutor;

    private TestCaseGroup testCaseGroup;

    public TestCaseStepAdapter(
            String resultRegistrationUrl,
            HttpStatus expectedResponseStatus,
            Supplier<ResponseEntity<?>> requestExecutor
    ) {
        this.resultRegistrationUrl = resultRegistrationUrl;
        this.expectedResponseStatus = expectedResponseStatus;
        this.expectedResponseFieldName = null;
        this.expectedResponse = null;
        this.requestExecutor = requestExecutor;
    }

    @Override
    public ResponseEntity<?> execute() {
        return requestExecutor.get();
    }

    public TestCaseStepAdapter inGroup(@NonNull TestCaseGroup group) {
        this.testCaseGroup = group;
        return this;
    }
}
