package com.gitlab.simsonic.itone2022.server.testcases;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface TestCaseStep {

    @NonNull
    String getResultRegistrationUrl();

    @NonNull
    HttpStatus getExpectedResponseStatus();

    @Nullable
    String getExpectedResponseFieldName();

    @Nullable
    Object getExpectedResponse();

    ResponseEntity<?> execute();

    @Nullable
    TestCaseGroup getTestCaseGroup();
}
