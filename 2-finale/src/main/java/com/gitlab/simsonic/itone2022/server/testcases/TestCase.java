package com.gitlab.simsonic.itone2022.server.testcases;

import java.util.List;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface TestCase {

    List<TestCaseStep> getSteps();
}
