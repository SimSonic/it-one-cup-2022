package com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.List;

@Value
@Builder
public class GetReportDataResponseDto {

    @With
    long reportId;
    int tableAmount;
    List<ReportDataTable> tables;

}
