package com.gitlab.simsonic.itone2022.server.remotedbclient;

import lombok.experimental.UtilityClass;

import java.util.Map;

import static java.util.Map.entry;

@UtilityClass
public class RemoteDbUrls {

    @UtilityClass
    public class Tables {

        public final String CREATE_TABLE = "/api/table/create-table";
        public final String GET_TABLE_BY_NAME = "/api/table/get-table-by-name/";
        public final String DROP_TABLE = "/api/table/drop-table/";
    }

    @UtilityClass
    public class TableQueries {

        public final String CREATE_TABLE_QUERY = "/api/table-query/add-new-query-to-table";
        public final String MODIFY_TABLE_QUERY = "/api/table-query/modify-query-in-table";
        public final String DELETE_TABLE_QUERY_BY_ID = "/api/table-query/delete-table-query-by-id/";
        public final String EXECUTE_TABLE_QUERY_BY_ID = "/api/table-query/execute-table-query-by-id/";
        public final String GET_TABLE_QUERY_BY_ID = "/api/table-query/get-table-query-by-id/";
        public final String GET_TABLE_QUERIES_BY_TABLE_NAME = "/api/table-query/get-all-queries-by-table-name/";
        public final String GET_ALL_TABLE_QUERIES = "/api/table-query/get-all-table-queries";
    }

    @UtilityClass
    public class SingleQueries {

        public final String CREATE_SINGLE_QUERY = "/api/single-query/add-new-query";
        public final String MODIFY_SINGLE_QUERY = "/api/single-query/modify-single-query";
        public final String DELETE_SINGLE_QUERY_BY_ID = "/api/single-query/delete-single-query-by-id/";
        public final String EXECUTE_SINGLE_QUERY_BY_ID = "/api/single-query/execute-single-query-by-id/";
        public final String GET_SINGLE_QUERY_BY_ID = "/api/single-query/get-single-query-by-id/";
        public final String GET_ALL_SINGLE_QUERIES = "/api/single-query/get-all-single-queries";
    }

    @UtilityClass
    public class Reports {

        public final String CREATE_REPORT = "/api/report/create-report";
        public final String GET_REPORT_BY_ID = "/api/report/get-report-by-id/";
    }

    private final Map<String, String> REGISTRATION_URLS = Map.ofEntries(
            entry(Tables.CREATE_TABLE, "/api/table/add-create-table-result"),
            entry(Tables.GET_TABLE_BY_NAME, "/api/table/add-get-table-by-name-result"),
            entry(Tables.DROP_TABLE, "/api/table/add-drop-table-result"),

            entry(TableQueries.CREATE_TABLE_QUERY, "/api/table-query/add-new-query-to-table-result"),
            entry(TableQueries.MODIFY_TABLE_QUERY, "/api/table-query/modify-table-query-by-id-result"),
            entry(TableQueries.GET_TABLE_QUERY_BY_ID, "/api/table-query/get-table-query-by-id-result"),
            entry(TableQueries.EXECUTE_TABLE_QUERY_BY_ID, "/api/table-query/execute-table-query-by-id-result"),
            entry(TableQueries.DELETE_TABLE_QUERY_BY_ID, "/api/table-query/delete-table-query-by-id-result"),
            // entry(TableQueries.GET_TABLE_QUERIES_BY_TABLE_NAME, ""),
            // entry(TableQueries.GET_ALL_TABLE_QUERIES, ""),

            entry(SingleQueries.CREATE_SINGLE_QUERY, "/api/single-query/add-new-query-result"),
            entry(SingleQueries.MODIFY_SINGLE_QUERY, "/api/single-query/add-modify-result"),
            entry(SingleQueries.GET_SINGLE_QUERY_BY_ID, "/api/single-query/add-get-single-query-by-id-result"),
            entry(SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID, "/api/single-query/add-execute-result"),
            entry(SingleQueries.DELETE_SINGLE_QUERY_BY_ID, "/api/single-query/add-delete-result"),

            entry(Reports.CREATE_REPORT, "/api/report/add-create-report-result"),
            entry(Reports.GET_REPORT_BY_ID, "/api/report/add-get-report-by-id-result")
    );

    public String getRegistrationFor(String method) {
        return REGISTRATION_URLS.get(method);
    }
}
