package com.gitlab.simsonic.itone2022.server.testcases.adapters;

import com.gitlab.simsonic.itone2022.server.testcases.TestCase;
import org.springframework.core.annotation.Order;

@Order(20)
public abstract class AbstractTableTestCase implements TestCase {

}
