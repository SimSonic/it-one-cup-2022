package com.gitlab.simsonic.itone2022.server.remotedbclient;

import lombok.experimental.UtilityClass;
import org.springframework.core.ParameterizedTypeReference;

import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.SingleQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.GetTableResponseDto;
import com.gitlab.simsonic.itone2022.server.common.ErrorResponseDto;

import java.util.List;

import static org.springframework.core.ParameterizedTypeReference.forType;

@UtilityClass
public class RemoteDbTypes {

    public final ParameterizedTypeReference<Void> VOID_TYPE = ParameterizedTypeReference.forType(Void.class);
    public final ParameterizedTypeReference<String> STRING_TYPE = ParameterizedTypeReference.forType(String.class);
    public final ParameterizedTypeReference<ErrorResponseDto> ERROR_RESPONSE_TYPE = forType(ErrorResponseDto.class);

    @UtilityClass
    public class Tables {

        public final ParameterizedTypeReference<GetTableResponseDto> GET_TABLE_BY_NAME_RESPONSE = ParameterizedTypeReference
                .forType(GetTableResponseDto.class);
    }

    @UtilityClass
    public class TableQueries {

        public final ParameterizedTypeReference<TableQueryResponseDto> TABLE_QUERY_RESPONSE = ParameterizedTypeReference
                .forType(TableQueryResponseDto.class);

        public final ParameterizedTypeReference<List<TableQueryResponseDto>> TABLE_QUERIES_LIST_RESPONSE = new ParameterizedTypeReference<>() {};
    }

    @UtilityClass
    public class SingleQueries {

        public final ParameterizedTypeReference<SingleQueryResponseDto> SINGLE_QUERY_RESPONSE = ParameterizedTypeReference
                .forType(TableQueryResponseDto.class);

        public final ParameterizedTypeReference<List<SingleQueryResponseDto>> SINGLE_QUERIES_LIST_RESPONSE = new ParameterizedTypeReference<>() {};
    }

    @UtilityClass
    public class Reports {

        public final ParameterizedTypeReference<GetReportDataResponseDto> GET_REPORT_RESPONSE_TYPE = ParameterizedTypeReference
                .forType(GetReportDataResponseDto.class);
    }
}
