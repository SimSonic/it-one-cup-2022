package com.gitlab.simsonic.itone2022.server.common;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.simsonic.itone2022.server.common.exceptions.CommonExceptions;
import com.gitlab.simsonic.itone2022.server.common.exceptions.base.CommonExceptionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Контроллер, обрабатывающий 404 и прочие стандартные ошибки.
 */
@RestController
@RequiredArgsConstructor
public class ApplicationErrorController implements ErrorController {

    private final CommonExceptionHelper commonExceptionHelper;

    @RequestMapping(value = "/error", method = {
            RequestMethod.GET,
            RequestMethod.POST,
            RequestMethod.PUT,
            RequestMethod.PATCH,
            RequestMethod.DELETE,
    })
    public void handleError(HttpServletRequest request, HttpServletResponse response) {
        String requestUri = StringUtils.getIfBlank(
                (String) request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI),
                request::getRequestURI);
        commonExceptionHelper.processCommonExceptionToResponse(
                response,
                CommonExceptions.NOT_FOUND.creator()
                        .message(requestUri)
                        .create());
    }
}
