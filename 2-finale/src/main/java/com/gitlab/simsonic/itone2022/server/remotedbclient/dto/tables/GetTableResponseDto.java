package com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class GetTableResponseDto {

    String tableName;
    int columnsAmount;
    List<ColumnInfo> columnInfos;
    String primaryKey;

    @Value
    @Builder
    public static class ColumnInfo {

        String title;
        String type;
    }
}
