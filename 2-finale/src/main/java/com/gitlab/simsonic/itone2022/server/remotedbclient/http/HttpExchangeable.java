package com.gitlab.simsonic.itone2022.server.remotedbclient.http;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

@FunctionalInterface
public interface HttpExchangeable {

    <T> ResponseEntity<T> exchange(RequestEntity<?> requestEntity, ParameterizedTypeReference<T> responseType);
}
