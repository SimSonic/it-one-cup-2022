package com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import com.gitlab.simsonic.itone2022.server.utils.NamingUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import java.util.List;

@Value
@With
@Builder
public class CreateTableRequestDto {

    @NotBlank
    @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
    @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
    String tableName;

    @NotNull
    @Positive
    Integer columnsAmount;

    @NotEmpty
    @Size(min = 1)
    List<@Valid ColumnInfo> columnInfos;

    @NotBlank
    @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
    String primaryKey;

    @Value
    @Builder
    public static class ColumnInfo {

        @NotBlank
        @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
        String title;

        @NotBlank
        String type;
    }
}
