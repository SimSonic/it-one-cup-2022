package com.gitlab.simsonic.itone2022.server.utils;

import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;

import com.gitlab.simsonic.itone2022.ItOneApplication;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.springframework.core.ParameterizedTypeReference.forType;

@UtilityClass
@Slf4j
public class TestUtils {

    public final ParameterizedTypeReference<Void> VOID_TYPE = forType(Void.class);
    public final ParameterizedTypeReference<String> STRING_TYPE = forType(String.class);

    private final ObjectMapper objectMapper = JsonMapper.builder()
            .enable(JsonReadFeature.ALLOW_JAVA_COMMENTS)
            .enable(SerializationFeature.INDENT_OUTPUT)
            .build()
            .findAndRegisterModules();

    /**
     * Достаёт байты из файла в ресурсах.
     */
    @SneakyThrows
    public byte[] getResourceBytes(String filename) {
        try (InputStream inputStream = ItOneApplication.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return Objects.requireNonNull(inputStream)
                    .readAllBytes();
        }
    }

    /**
     * Достаёт объект указанного класса из JSON-файла в ресурсах.
     */
    @SneakyThrows
    public <T> T getResource(String filename, Class<T> clazz) {
        try (InputStream inputStream = ItOneApplication.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return objectMapper.readValue(inputStream, clazz);
        }
    }

    /**
     * Достаёт объект указанного типа из JSON-файла в ресурсах.
     */
    @SneakyThrows
    public <T> T getResource(String filename, TypeReference<T> type) {
        try (InputStream inputStream = ItOneApplication.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return objectMapper.readValue(inputStream, type);
        }
    }

    /**
     * Читает файл в ресурсах в строку.
     */
    public String getResourceAsString(String filename) {
        try (InputStream inputStream = ItOneApplication.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return new String(Objects.requireNonNull(inputStream).readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            log.error("Cannot read resource '{}': {}", filename, ex.getMessage(), ex);
        }

        return null;
    }

    @SneakyThrows
    private void assertResourceInputStream(InputStream inputStream, String filename) {
        Objects.requireNonNull(inputStream);
        if (inputStream.available() == 0) {
            throw new IllegalStateException("InputStream is empty, filename = " + filename);
        }
    }
}
