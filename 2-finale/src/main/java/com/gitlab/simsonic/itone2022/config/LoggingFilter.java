package com.gitlab.simsonic.itone2022.config;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@Component
@RequiredArgsConstructor
@Slf4j
class LoggingFilter extends OncePerRequestFilter {

    private static final Set<HttpMethod> LOGGING_METHODS = Set.of(
            HttpMethod.POST,
            HttpMethod.PUT,
            HttpMethod.PATCH);

    private static final Set<String> LOGGING_URIS = Set.of(
            "/api/table/",
            "/api/table-query/",
            "/api/single-query/",
            "/api/report/"
    );

    private final AtomicLong requestCounter = new AtomicLong();

    @SneakyThrows
    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) {
        long requestId = requestCounter.incrementAndGet();

        String requestURI = request.getRequestURI();
        String method = request.getMethod();
        log.debug("REQ # {} --> {} {}", requestId, method, requestURI);

        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

        filterChain.doFilter(requestWrapper, responseWrapper);

        if (isRequestPayloadShouldBeLogged(request)) {
            // Выводим тело запроса.
            byte[] payloadBytes = requestWrapper.getContentAsByteArray();
            String payloadString = new String(payloadBytes, StandardCharsets.UTF_8);
            if (!payloadString.isBlank()) {
                log.debug("REQ # {} --> {}", requestId, payloadString);
            }
        }

        HttpStatus httpStatus = HttpStatus.valueOf(response.getStatus());
        log.debug("REQ # {} <-- {}", requestId, httpStatus);

        if (isResponsePayloadShouldBeLogged(request)) {
            // Выводим тело ответа.
            byte[] payloadBytes = responseWrapper.getContentInputStream().readAllBytes();
            String payloadString = new String(payloadBytes, StandardCharsets.UTF_8);
            if (!payloadString.isBlank()) {
                log.debug("REQ # {} <-- {}", requestId, payloadString);
            }
        }

        responseWrapper.copyBodyToResponse();
    }

    private static boolean isRequestPayloadShouldBeLogged(HttpServletRequest request) {
        HttpMethod httpMethod = HttpMethod.valueOf(request.getMethod());
        boolean logByMethod = LOGGING_METHODS.contains(httpMethod);

        return logByMethod && isResponsePayloadShouldBeLogged(request);
    }

    private static boolean isResponsePayloadShouldBeLogged(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        return LOGGING_URIS.stream().anyMatch(requestURI::startsWith);
    }
}
