package com.gitlab.simsonic.itone2022.server.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NamingUtils {

    public static final String IDENTIFIER_PATTERN = "[a-zA-Z]+[_a-zA-Z0-9]*";

    public static final int MAX_TABLE_NAME = 50;

    public static final int MAX_QUERY_LENGTH = 120;
}
