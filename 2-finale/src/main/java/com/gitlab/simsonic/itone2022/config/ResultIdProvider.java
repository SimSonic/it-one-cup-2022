package com.gitlab.simsonic.itone2022.config;

import java.util.concurrent.Callable;

public interface ResultIdProvider {

    int getNextUniqueId();

    <V> V executeWithinNewResultId(Callable<V> callable);

    Integer getCurrentResultId();

    <V> V executeRegistration(Callable<V> callable);

    boolean isInsideRegistration();
}
