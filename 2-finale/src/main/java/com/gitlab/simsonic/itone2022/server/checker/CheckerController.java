package com.gitlab.simsonic.itone2022.server.checker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CheckerController {

    private final CheckerService checkerService;

    @GetMapping("/api/start")
    void runTests() {
        checkerService.runAllTestCases();
    }
}
