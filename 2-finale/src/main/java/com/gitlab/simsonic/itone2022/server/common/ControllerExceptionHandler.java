package com.gitlab.simsonic.itone2022.server.common;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.gitlab.simsonic.itone2022.server.common.exceptions.CommonExceptions;
import com.gitlab.simsonic.itone2022.server.common.exceptions.base.CommonException;
import com.gitlab.simsonic.itone2022.server.common.exceptions.base.CommonExceptionHelper;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

import java.sql.SQLSyntaxErrorException;

/**
 * Обработка исключений, вылетающих из контроллеров.
 */
@RestControllerAdvice
@RequiredArgsConstructor
public class ControllerExceptionHandler {

    private final CommonExceptionHelper commonExceptionHelper;

    /**
     * Ожидаемое исключение. Вероятно, было брошено в коде вручную.
     */
    @ExceptionHandler(CommonException.class)
    public void onCommonException(HttpServletResponse response, CommonException exception) {
        commonExceptionHelper.processCommonExceptionToResponse(response, exception);
    }

    /**
     * Кто-то перепутал GET и PUT.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponseDto onError(HttpRequestMethodNotSupportedException exception) {
        return commonExceptionHelper.buildErrorResponseAndLogIt(CommonExceptions.UNSUPPORTED, exception);
    }

    /**
     * Сработала валидация на какой-нибудь @Valid DTO.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponseDto onError(MethodArgumentNotValidException exception) {
        return commonExceptionHelper.buildErrorResponse(exception);
    }

    /**
     * Кастомная валидация.
     */
    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponseDto onError(ValidationException exception) {
        return commonExceptionHelper.buildErrorResponseAndLogIt(CommonExceptions.VALIDATION_FAILED, exception);
    }

    /**
     * Jackson сфейлился.
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponseDto onError(HttpMessageNotReadableException exception) {
        return commonExceptionHelper.buildErrorResponseAndLogIt(CommonExceptions.VALIDATION_FAILED, exception);
    }

    /**
     * PathVariable сфейлился.
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponseDto onError(MethodArgumentTypeMismatchException exception) {
        return commonExceptionHelper.buildErrorResponseAndLogIt(CommonExceptions.VALIDATION_FAILED, exception);
    }

    /**
     * SQL сфейлился.
     */
    @ExceptionHandler(SQLSyntaxErrorException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponseDto onError(SQLSyntaxErrorException exception) {
        return commonExceptionHelper.buildErrorResponseAndLogIt(CommonExceptions.INTERNAL_SERVER_WARNING, exception);
    }

    /**
     * Произошло что-то совсем неожиданное.
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponseDto onError(Exception exception) {
        return commonExceptionHelper.buildErrorResponseAndLogIt(CommonExceptions.INTERNAL_SERVER_ERROR, exception);
    }
}
