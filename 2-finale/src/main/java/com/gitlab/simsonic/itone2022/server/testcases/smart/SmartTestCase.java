package com.gitlab.simsonic.itone2022.server.testcases.smart;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gitlab.simsonic.itone2022.config.ResultIdProvider;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbClient;
import com.gitlab.simsonic.itone2022.server.remotedbclient.RemoteDbUrls;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.CreateReportRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.reports.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.AddNewSingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.single_queries.ModifySingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.table_queries.AddNewQueryToTableRequestDto;
import com.gitlab.simsonic.itone2022.server.remotedbclient.dto.tables.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseGroup;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStep;
import com.gitlab.simsonic.itone2022.server.testcases.TestCaseStepAdapter;
import com.gitlab.simsonic.itone2022.server.testcases.adapters.AbstractTableTestCase;
import com.gitlab.simsonic.itone2022.server.utils.TestUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class SmartTestCase extends AbstractTableTestCase {

    private static final String TABLE_NAME_OLOLO = "Customer_Ололо";
    private static final String TABLE_NAME_BUGAGA = "Customer_БуГаГа";

    private final RemoteDbClient remoteDbClient;
    private final ResultIdProvider resultIdProvider;

    private String getOldTableName() {
        return TABLE_NAME_OLOLO + resultIdProvider.getNextUniqueId();
    }

    private String getNewTableName() {
        return TABLE_NAME_OLOLO + resultIdProvider.getNextUniqueId();
    }

    @Override
    public List<TestCaseStep> getSteps() {
        String oldTableName = getOldTableName();
        String newTableName = getNewTableName();

        // Создание таблицы.
        TestCaseStep createTable = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                HttpStatus.CREATED,
                () -> remoteDbClient.createTable(
                        TestUtils
                                .getResource("/tests/create-table-OLOLO-request.json", CreateTableRequestDto.class)
                                .withTableName(oldTableName)))
                .inGroup(TestCaseGroup.TABLE);

        // Вставляем в таблицу одну единственную запись без NULL.
        long singleQueryId = resultIdProvider.getNextUniqueId();
        String singleQueryIdStr = String.valueOf(singleQueryId);

        String insertIntoTableLine1SQL = "INSERT INTO %s (ID, Имя) VALUES (1, 'Реальный упырь')"
                .formatted(oldTableName);
        TestCaseStep createInsertIntoNewTableSingleQueryLine1 = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.CREATE_SINGLE_QUERY),
                HttpStatus.CREATED,
                () -> remoteDbClient.createSingleQuery(
                        AddNewSingleQueryRequestDto.builder()
                                .queryId(singleQueryId)
                                .query(insertIntoTableLine1SQL)
                                .build()))
                .inGroup(TestCaseGroup.SINGLE_QUERY);

        // Запуск SQL запроса для вставки в таблицу.
        TestCaseStep executeSingleQuery = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.EXECUTE_SINGLE_QUERY_BY_ID),
                HttpStatus.CREATED,
                () -> remoteDbClient.executeSingleQuery(singleQueryIdStr))
                .inGroup(TestCaseGroup.SINGLE_QUERY);

        // SQL запрос для переименования таблицы.
        long tableQueryId = resultIdProvider.getNextUniqueId();
        String tableQueryIdStr = String.valueOf(tableQueryId);

        String renameTableSQL = "ALTER TABLE %s RENAME TO %s; ALTER TABLE %s ALTER COLUMN имя RENAME TO ФИО;"
                .formatted(oldTableName, newTableName, newTableName);
        TestCaseStep createRenameTableTableQuery = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.CREATE_TABLE_QUERY),
                HttpStatus.CREATED,
                () -> remoteDbClient.createTableQuery(
                        AddNewQueryToTableRequestDto.builder()
                                .tableName(oldTableName)
                                .queryId(tableQueryId)
                                .query(renameTableSQL)
                                .build()))
                .inGroup(TestCaseGroup.TABLE_QUERY);

        // Запуск SQL запроса для переименования таблицы.
        TestCaseStep executeRenameTableTableQuery = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.EXECUTE_TABLE_QUERY_BY_ID),
                HttpStatus.CREATED,
                () -> remoteDbClient.executeTableQuery(tableQueryIdStr))
                .inGroup(TestCaseGroup.TABLE_QUERY);

        // Получение таблицы по новому имени.
        TestCaseStep getTableByNewName = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.GET_TABLE_BY_NAME),
                HttpStatus.OK,
                "table",
                TestUtils
                        .getResource("/tests/create-table-BUGAGA-expected.json", CreateTableRequestDto.class)
                        .withTableName(newTableName),
                () -> remoteDbClient.getTable(newTableName))
                .inGroup(TestCaseGroup.TABLE);

        // Получение SQL запроса на переименование таблицы по новому имени таблицы.
        TestCaseStep getTableQueryWithNewTableName = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.GET_TABLE_QUERY_BY_ID),
                HttpStatus.OK,
                "tableQuery",
                AddNewQueryToTableRequestDto.builder()
                        .tableName(newTableName)
                        .queryId(tableQueryId)
                        .query(renameTableSQL)
                        .build(),
                () -> remoteDbClient.getTableQuery(tableQueryIdStr))
                .inGroup(TestCaseGroup.TABLE_QUERY);

        // Вставляем в таблицу вторую запись, содержащую NULL.
        String insertIntoTableLine2SQL = "INSERT INTO %s (ID) (3)"
                .formatted(newTableName);
        TestCaseStep modifyInsertIntoNewTableSingleQueryLine2 = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.SingleQueries.MODIFY_SINGLE_QUERY),
                HttpStatus.OK,
                () -> remoteDbClient.modifySingleQuery(
                        ModifySingleQueryRequestDto.builder()
                                .queryId(singleQueryId)
                                .query(insertIntoTableLine2SQL)
                                .build()))
                .inGroup(TestCaseGroup.SINGLE_QUERY);

        // Создание таблицы с новым именем, должен быть FAIL.
        TestCaseStep createTableWithNewName = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.CREATE_TABLE),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.createTable(
                        TestUtils
                                .getResource("/tests/create-table-OLOLO-request.json", CreateTableRequestDto.class)
                                .withTableName(newTableName)))
                .inGroup(TestCaseGroup.TABLE);

        // Ну и удалим её тут же.
        TestCaseStep dropTableByNewName = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                HttpStatus.CREATED,
                () -> remoteDbClient.dropTable(newTableName))
                .inGroup(TestCaseGroup.TABLE);

        // Следом попытка удалить запрос таблицы, но его уже не должно существовать.
        TestCaseStep dropTableQuery = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.TableQueries.DELETE_TABLE_QUERY_BY_ID),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.deleteTableQuery(tableQueryIdStr))
                .inGroup(TestCaseGroup.TABLE_QUERY);

        // Должна быть неудачная попытка удалить таблицу по старому имени.
        TestCaseStep dropTableByOldName = new TestCaseStepAdapter(
                RemoteDbUrls.getRegistrationFor(RemoteDbUrls.Tables.DROP_TABLE),
                HttpStatus.NOT_ACCEPTABLE,
                () -> remoteDbClient.dropTable(oldTableName))
                .inGroup(TestCaseGroup.TABLE);

        return List.of(
                createTable,

                createInsertIntoNewTableSingleQueryLine1, executeSingleQuery,

                createRenameTableTableQuery, executeRenameTableTableQuery,

                getTableByNewName,
                getTableQueryWithNewTableName,

                modifyInsertIntoNewTableSingleQueryLine2, executeSingleQuery,

                createTableWithNewName,
                dropTableByNewName,
                dropTableQuery,
                dropTableByOldName
        );
    }
}
