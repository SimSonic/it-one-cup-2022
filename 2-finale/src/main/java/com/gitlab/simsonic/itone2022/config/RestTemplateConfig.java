package com.gitlab.simsonic.itone2022.config;

import lombok.RequiredArgsConstructor;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.NonNull;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
@RequiredArgsConstructor
class RestTemplateConfig {

    private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(1);
    private static final Duration DEFAULT_READ_TIMEOUT = Duration.ofSeconds(5);

    /**
     * Примерное кол-во хостов на один http-клиент.
     */
    private static final int MAX_HOSTS_PER_CLIENT = 100;

    /**
     * Максимальное кол-во соединений на один хост.
     */
    private static final int MAX_CONNECTIONS_PER_ROUTE = 10;

    @Bean
    @Qualifier("restTemplateNoOp")
    RestTemplate bankNetworkNoOpRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return createNoOpRestTemplate(restTemplateBuilder);
    }

    private static RestTemplate createNoOpRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        var restTemplate = createDefaultRestTemplate(restTemplateBuilder);
        restTemplate.setErrorHandler(NoOpResponseErrorHandler.INSTANCE);
        return restTemplate;
    }

    @Bean
    @Qualifier("restTemplate")
    RestTemplate bankNetworkRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return createDefaultRestTemplate(restTemplateBuilder);
    }

    private static RestTemplate createDefaultRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        var httpClient = createBankNetworkHttpClient();
        var requestFactory = clientToFactory(httpClient);
        return restTemplateBuilder
                .requestFactory(() -> requestFactory)
                .setConnectTimeout(DEFAULT_CONNECT_TIMEOUT)
                .setReadTimeout(DEFAULT_READ_TIMEOUT)
                .build();
    }

    /**
     * HttpComponentsClientHttpRequestFactory используется по той причине, что клиент, созданный стандартной
     * фабрикой SimpleClientHttpRequestFactory даже в случае использования NoOpResponseErrorHandler-а
     * при получении статуса 401 UNAUTHORIZED будет бросать исключение.
     */
    private static ClientHttpRequestFactory clientToFactory(HttpClient httpClient) {
        return new HttpComponentsClientHttpRequestFactory(httpClient);
    }

    private static HttpClient createBankNetworkHttpClient() {
        var connectionManager = createPoolingConnectionManager();
        return HttpClientBuilder.create()
                // Отключаем обработку куки.
                .disableCookieManagement()
                // Указываем настроенный Connection Manager.
                .setConnectionManager(connectionManager)
                .build();
    }

    private static HttpClientConnectionManager createPoolingConnectionManager() {
        var connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(MAX_HOSTS_PER_CLIENT * MAX_CONNECTIONS_PER_ROUTE);
        connectionManager.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
        return connectionManager;
    }

    private static class NoOpResponseErrorHandler extends DefaultResponseErrorHandler {

        /**
         * Обработчик HTTP-статусов ответа, который не выбрасывает исключения
         * <b>HttpClientErrorException</b> и <b>HttpServerErrorException</b>.
         */
        private static final ResponseErrorHandler INSTANCE = new NoOpResponseErrorHandler();

        @Override
        public void handleError(@NonNull ClientHttpResponse response) {}
    }
}
