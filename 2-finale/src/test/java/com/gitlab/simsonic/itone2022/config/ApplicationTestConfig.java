package com.gitlab.simsonic.itone2022.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.gitlab.simsonic.itone2022.server.remotedbclient.http.HttpExchangeable;

@TestConfiguration
@RequiredArgsConstructor
@Slf4j
public class ApplicationTestConfig {

    @Primary
    @Bean
    HttpExchangeable testHttpExchangeable(@Autowired TestRestTemplate testRestTemplate) {
        // Потому что стандартный HttpClient при получении 401 бросает исключение.
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        testRestTemplate.getRestTemplate()
                .setRequestFactory(requestFactory);

        return testRestTemplate::exchange;
    }
}
