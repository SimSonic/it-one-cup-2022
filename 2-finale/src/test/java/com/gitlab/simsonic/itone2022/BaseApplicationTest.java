package com.gitlab.simsonic.itone2022;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gitlab.simsonic.itone2022.config.ApplicationTestConfig;
import com.gitlab.simsonic.itone2022.server.remotedbclient.http.RestApiHttpClient;

@SpringBootTest(
        classes = {
                ItOneApplication.class,
                ApplicationTestConfig.class,
        },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@Slf4j
public class BaseApplicationTest {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Клиент для API, предоставляемого сервером.
     */
    @Getter
    @Autowired
    private RestApiHttpClient restApiHttpClient;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @SneakyThrows
    protected String toJson(Object anyDataObject) {
        return objectMapper.writeValueAsString(anyDataObject);
    }

    @SneakyThrows
    protected <T> T fromJson(String json, Class<T> clazz) {
        return objectMapper.readValue(json, clazz);
    }
}
