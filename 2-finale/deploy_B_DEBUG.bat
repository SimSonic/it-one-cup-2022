@SET JAVA_HOME=C:\Program Files\BellSoft\LibericaJDK-17

CALL mvnw clean verify

docker build --tag stor.highloadcup.ru/it_one_j22_final/sleepy_mongoose .

docker push stor.highloadcup.ru/it_one_j22_final/sleepy_mongoose
