package com.gitlab.simsonic.itone2022.tests.tables;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;
import com.gitlab.simsonic.itone2022.server.tables.api.GetTableResponseDto;
import com.gitlab.simsonic.itone2022.utils.TestUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class GetTableTests extends BaseApiTest {

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @ParameterizedTest
    @CsvSource(
            textBlock = """
                    /tests/tables/1-create-table-customer.json; Customer; /tests/tables/5-get-table-customer.json;
                    /tests/tables/2-create-table-artists.json; Artists; /tests/tables/6-get-table-artists.json;
                    """,
            delimiter = ';')
    void testSuccess(String jsonSource, String tableName, String jsonAnswer) {
        executeCreateTable(jsonSource, HttpStatus.CREATED);

        GetTableResponseDto responseDto = executeGetTable(tableName);
        GetTableResponseDto expected = TestUtils.getResource(jsonAnswer, GetTableResponseDto.class);
        assertThat(responseDto).isEqualTo(expected);
    }

    @Test
    void testTableNotExists() {
        GetTableResponseDto responseDto = executeGetTable("not-existing-table");
        assertThat(responseDto).isNull();
    }
}
