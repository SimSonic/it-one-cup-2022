package com.gitlab.simsonic.itone2022.tests.reports;

import com.gitlab.simsonic.itone2022.BaseApiTest;
import com.gitlab.simsonic.itone2022.server.table_queries.service.QueryExecutionService;
import com.gitlab.simsonic.itone2022.server.reports.api.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.utils.TestUtils;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.assertThat;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GetReportDataTests extends BaseApiTest {

    @Autowired
    private QueryExecutionService queryExecutionService;

    @Test
    void testSuccess() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);
        executeCreateTable("/tests/reports/create-table-job.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);

        executeCreateReport("/tests/reports/create-report-1-ok.json", HttpStatus.CREATED);
        executeCreateReport("/tests/reports/create-report-2-ok.json", HttpStatus.CREATED);

        insertTestData();

        GetReportDataResponseDto actual = executeGetReportData("2", HttpStatus.CREATED);
        GetReportDataResponseDto expected = TestUtils.getResource(
                "/tests/reports/get-report-data-expected.json",
                GetReportDataResponseDto.class);

        assertThat(actual)
                .isNotNull()
                .isEqualTo(expected);
    }

    private void insertTestData() {
        queryExecutionService.sqlExecute("INSERT INTO Artists (id) VALUES (10)");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id) VALUES (11)");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id) VALUES (12)");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id) VALUES (13)");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id) VALUES (14)");

        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (20)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (21)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (22)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (23)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (24)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (25)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (26)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (27)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (28)");
        queryExecutionService.sqlExecute("INSERT INTO Job (id) VALUES (29)");
    }
}
