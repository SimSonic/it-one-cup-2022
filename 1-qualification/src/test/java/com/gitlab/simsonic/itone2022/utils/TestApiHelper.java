package com.gitlab.simsonic.itone2022.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.gitlab.simsonic.itone2022.server.common.ErrorResponseDto;

import java.net.URI;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Реализация GET, POST и остальных вызовов к контроллерам приложения.
 */
@Component
@RequiredArgsConstructor
@Getter
@Slf4j
class TestApiHelper {

    private final MultiValueMap<String, String> headers = new HttpHeaders();

    private final TestRestTemplate testRestTemplate;

    @NonNull
    public <DTO> DTO get(String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = getExchange(method, responseType);
        return assertNormalResponseEntity(response);
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> getWithError(String method) {
        ResponseEntity<ErrorResponseDto> response = getExchange(method, TestUtils.ERROR_RESPONSE_TYPE);
        return assertErrorResponseEntity(response);
    }

    @NonNull
    public <DTO> DTO post(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = postExchange(requestDto, method, responseType);
        return assertNormalResponseEntity(response);
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> postWithError(Object requestDto, String method) {
        ResponseEntity<ErrorResponseDto> response = postExchange(requestDto, method, TestUtils.ERROR_RESPONSE_TYPE);
        return assertErrorResponseEntity(response);
    }

    @NonNull
    public <DTO> DTO put(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = putExchange(requestDto, method, responseType);
        return assertNormalResponseEntity(response);
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> putWithError(Object requestDto, String method) {
        ResponseEntity<ErrorResponseDto> response = putExchange(requestDto, method, TestUtils.ERROR_RESPONSE_TYPE);
        return assertErrorResponseEntity(response);
    }

    @NonNull
    public <DTO> DTO delete(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        ResponseEntity<DTO> response = deleteExchange(requestDto, method, responseType);
        return assertNormalResponseEntity(response);
    }

    @NonNull
    public ResponseEntity<ErrorResponseDto> deleteWithError(Object requestDto, String method) {
        ResponseEntity<ErrorResponseDto> response = deleteExchange(requestDto, method, TestUtils.ERROR_RESPONSE_TYPE);
        return assertErrorResponseEntity(response);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> getExchange(String method, ParameterizedTypeReference<DTO> responseType) {
        headers.remove(HttpHeaders.CONTENT_TYPE);
        return exchange(HttpMethod.GET, method, null, responseType);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> postExchange(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return exchange(HttpMethod.POST, method, requestDto, responseType);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> putExchange(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return exchange(HttpMethod.PUT, method, requestDto, responseType);
    }

    @NonNull
    public <DTO> ResponseEntity<DTO> deleteExchange(Object requestDto, String method, ParameterizedTypeReference<DTO> responseType) {
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return exchange(HttpMethod.DELETE, method, requestDto, responseType);
    }

    @NonNull
    private <DTO> ResponseEntity<DTO> exchange(
            HttpMethod httpMethod,
            String methodUri,
            Object request,
            ParameterizedTypeReference<DTO> responseType
    ) {
        try {
            URI uri = URI.create(methodUri);
            log.info("==> {} {}", httpMethod, uri);
            headers.forEach((k, v) -> log.debug("==> HEADER {} = {}", k, StreamEx.of(v).joining(", ")));
            if (request != null) {
                log.info("==> BODY: {}", request);
            }
            RequestEntity<Object> entity = new RequestEntity<>(request, headers, httpMethod, uri);
            ResponseEntity<DTO> responseEntity = testRestTemplate.exchange(entity, responseType);
            HttpStatus statusCode = responseEntity.getStatusCode();
            if (!statusCode.is2xxSuccessful()) {
                log.info("<== RESPONSE STATUS {}, PAYLOAD: {}", statusCode.value(), responseEntity.getBody());
            }
            return responseEntity;
        } finally {
            log.info("<== REQUEST FINISHED.");
        }
    }

    @NonNull
    private static <DTO> DTO assertNormalResponseEntity(ResponseEntity<DTO> response) {
        assertThat(response)
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(HttpStatus.OK);

        DTO body = response.getBody();
        assertThat(body).isNotNull();

        return body;
    }

    @NonNull
    private static ResponseEntity<ErrorResponseDto> assertErrorResponseEntity(ResponseEntity<ErrorResponseDto> response) {
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isNotEqualTo(HttpStatus.OK);

        ErrorResponseDto body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body.isError()).isTrue();

        return response;
    }
}
