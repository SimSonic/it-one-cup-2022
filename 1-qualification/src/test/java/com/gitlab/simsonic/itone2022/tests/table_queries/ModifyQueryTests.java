package com.gitlab.simsonic.itone2022.tests.table_queries;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ModifyQueryTests extends BaseApiTest {

    @BeforeEach
    void beforeEach() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);
    }

    @Test
    void testSuccess() {
        executeModifyQuery("/tests/table_queries/modify-1-ok.json", HttpStatus.OK);
        executeModifyQuery("/tests/table_queries/modify-2-ok.json", HttpStatus.OK);
        executeModifyQuery("/tests/table_queries/modify-3-ok.json", HttpStatus.OK);
    }

    @Test
    void testErrors() {
        executeModifyQuery("/tests/table_queries/modify-1-error.json", HttpStatus.NOT_ACCEPTABLE);
        executeModifyQuery("/tests/table_queries/modify-12a-error.json", HttpStatus.NOT_ACCEPTABLE);
    }

    @Disabled
    @Test
    void testShouldBeErrorButSpecLies() {
        // executeModifyQuery("/tests/table_queries/modify-1-error.json", HttpStatus.NOT_ACCEPTABLE);
        executeModifyQuery("/tests/table_queries/modify-1-error.json", HttpStatus.OK);
        executeModifyQuery("/tests/table_queries/modify-12a-error.json", HttpStatus.NOT_ACCEPTABLE);
    }
}
