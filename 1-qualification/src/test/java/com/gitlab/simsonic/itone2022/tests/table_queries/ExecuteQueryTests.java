package com.gitlab.simsonic.itone2022.tests.table_queries;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ExecuteQueryTests extends BaseApiTest {

    @BeforeEach
    void beforeEach() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);
    }

    @Test
    void testSuccess() {
        executeRunTableQuery("2", HttpStatus.CREATED);
        executeRunTableQuery("3", HttpStatus.CREATED);
    }

    @Test
    void testErrors() {
        executeRunTableQuery("0", HttpStatus.NOT_ACCEPTABLE);
        executeRunTableQuery("1", HttpStatus.NOT_ACCEPTABLE);
        executeRunTableQuery("string", HttpStatus.NOT_ACCEPTABLE);
    }
}
