package com.gitlab.simsonic.itone2022.tests.tables;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;
import com.gitlab.simsonic.itone2022.server.tables.api.GetTableResponseDto;

import static org.assertj.core.api.Assertions.assertThat;

public class DropTableTests extends BaseApiTest {

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @ParameterizedTest
    @CsvSource(
            textBlock = """
                    /tests/tables/1-create-table-customer.json; Customer;
                    /tests/tables/2-create-table-artists.json; Artists;
                    """,
            delimiter = ';')
    void testSuccess(String jsonSource, String tableName) {
        executeCreateTable(jsonSource, HttpStatus.CREATED);
        executeDropTable(tableName, HttpStatus.CREATED);

        GetTableResponseDto responseDto = executeGetTable(tableName);
        assertThat(responseDto).isNull();
    }

    @Test
    void testTableNotExists() {
        executeDropTable("not-existing-table", HttpStatus.NOT_ACCEPTABLE);
    }
}
