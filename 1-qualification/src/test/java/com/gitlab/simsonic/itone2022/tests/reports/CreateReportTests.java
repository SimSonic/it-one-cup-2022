package com.gitlab.simsonic.itone2022.tests.reports;

import com.gitlab.simsonic.itone2022.BaseApiTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CreateReportTests extends BaseApiTest {

    @BeforeEach
    void beforeEach() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);
        executeCreateTable("/tests/reports/create-table-job.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);
    }

    @Test
    void testSuccess() {
        executeCreateReport("/tests/reports/create-report-1-ok.json", HttpStatus.CREATED);
        executeCreateReport("/tests/reports/create-report-2-ok.json", HttpStatus.CREATED);
    }

    @Test
    void testErrors() {
        executeCreateReport("/tests/reports/create-report-1-error.json", HttpStatus.NOT_ACCEPTABLE);
        executeCreateReport("/tests/reports/create-report-2-error.json", HttpStatus.NOT_ACCEPTABLE);
    }
}
