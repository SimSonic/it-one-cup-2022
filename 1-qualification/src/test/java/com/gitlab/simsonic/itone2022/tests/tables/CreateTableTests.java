package com.gitlab.simsonic.itone2022.tests.tables;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;

public class CreateTableTests extends BaseApiTest {

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @ParameterizedTest
    @ValueSource(strings = {
            "/tests/tables/1-create-table-customer.json",
            "/tests/tables/2-create-table-artists.json",
    })
    void testSuccess(String jsonSource) {
        executeCreateTable(jsonSource, HttpStatus.CREATED);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "/tests/tables/3-create-table-error.json",
            "/tests/tables/4-create-table-error.json",
    })
    void testErrors(String jsonSource) {
        executeCreateTable(jsonSource, HttpStatus.NOT_ACCEPTABLE);
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @Test
    void testDuplicate() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.NOT_ACCEPTABLE);
    }
}
