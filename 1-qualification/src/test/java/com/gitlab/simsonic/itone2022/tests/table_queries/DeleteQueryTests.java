package com.gitlab.simsonic.itone2022.tests.table_queries;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DeleteQueryTests extends BaseApiTest {

    @BeforeEach
    void beforeEach() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);
    }

    @Test
    void testSuccess() {
        executeDeleteQuery("1", HttpStatus.ACCEPTED);
        executeDeleteQuery("2", HttpStatus.ACCEPTED);
        executeDeleteQuery("3", HttpStatus.ACCEPTED);
    }

    @Test
    void testErrors() {
        executeDeleteQuery("string", HttpStatus.NOT_ACCEPTABLE);
        executeDeleteQuery("11", HttpStatus.NOT_ACCEPTABLE);
    }
}
