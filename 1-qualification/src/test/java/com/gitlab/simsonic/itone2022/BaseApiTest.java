package com.gitlab.simsonic.itone2022;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gitlab.simsonic.itone2022.server.reports.api.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.server.table_queries.api.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.tables.api.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.tables.api.GetTableResponseDto;
import com.gitlab.simsonic.itone2022.utils.TestUtils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BaseApiTest extends BaseApplicationTest {

    private static final ParameterizedTypeReference<GetTableResponseDto> GET_TABLE_RESPONSE_TYPE = ParameterizedTypeReference
            .forType(GetTableResponseDto.class);

    private static final ParameterizedTypeReference<GetReportDataResponseDto> GET_REPORT_RESPONSE_TYPE = ParameterizedTypeReference
            .forType(GetReportDataResponseDto.class);

    private static final ParameterizedTypeReference<List<TableQueryResponseDto>> LIST_QUERIES_RESPONSE_TYPE = new ParameterizedTypeReference<>() {};

    protected void executeCreateTable(String filename, HttpStatus expectedStatus) {
        CreateTableRequestDto requestDto = TestUtils.getResource(filename, CreateTableRequestDto.class);
        ResponseEntity<Void> responseEntity = getTestApi()
                .postForEntity(requestDto, "/api/table/create-table", TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected GetTableResponseDto executeGetTable(String tableName) {
        ResponseEntity<GetTableResponseDto> responseEntity = getTestApi()
                .getForEntity("/api/table/get-table-by-name/" + tableName, GET_TABLE_RESPONSE_TYPE);

        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(HttpStatus.OK);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return responseEntity.getBody();
        }

        return null;
    }

    protected void executeDropTable(String tableName, HttpStatus expectedStatus) {
        ResponseEntity<Void> responseEntity = getTestApi()
                .deleteForEntity(null, "/api/table/drop-table-by-name/" + tableName, TestUtils.VOID_TYPE);

        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected void executeCreateQuery(String filename, HttpStatus expectedStatus) {
        String requestJson = TestUtils.getResourceAsString(filename);
        ResponseEntity<Void> responseEntity = getTestApi()
                .postForEntity(requestJson, "/api/table-query/add-new-query-to-table", TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected void executeModifyQuery(String filename, HttpStatus expectedStatus) {
        String requestJson = TestUtils.getResourceAsString(filename);
        ResponseEntity<Void> responseEntity = getTestApi()
                .putForEntity(requestJson, "/api/table-query/modify-query-in-table", TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected void executeDeleteQuery(String queryId, HttpStatus expectedStatus) {
        ResponseEntity<Void> responseEntity = getTestApi()
                .deleteForEntity(null, "/api/table-query/delete-table-query-by-id/" + queryId, TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected TableQueryResponseDto executeGetQuery(String queryId, HttpStatus expectedStatus) {
        ResponseEntity<String> responseEntity = getTestApi()
                .getForEntity("/api/table-query/get-table-query-by-id/" + queryId, TestUtils.STRING_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);

        String body = responseEntity.getBody();
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return fromJson(body, TableQueryResponseDto.class);
        }

        return null;
    }

    protected List<TableQueryResponseDto> executeListTableQueries(String tableName) {
        ResponseEntity<List<TableQueryResponseDto>> responseEntity = getTestApi()
                .getForEntity("/api/table-query/get-all-queries-by-table-name/" + tableName, LIST_QUERIES_RESPONSE_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(HttpStatus.OK);

        return responseEntity.getBody();
    }

    protected List<TableQueryResponseDto> executeListAllQueries() {
        ResponseEntity<List<TableQueryResponseDto>> responseEntity = getTestApi()
                .getForEntity("/api/table-query/get-all-table-queries", LIST_QUERIES_RESPONSE_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(HttpStatus.OK);

        return responseEntity.getBody();
    }

    protected void executeRunTableQuery(String queryId, HttpStatus expectedStatus) {
        ResponseEntity<Void> responseEntity = getTestApi()
                .getForEntity("/api/table-query/execute-table-query-by-id/" + queryId, TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected void executeRunSingleQuery(String queryId) {
        ResponseEntity<Void> responseEntity = getTestApi()
                .getForEntity("/api/single-query/execute-single-query-by-id/" + queryId, TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(HttpStatus.CREATED);
    }

    protected void executeCreateReport(String filename, HttpStatus expectedStatus) {
        String requestJson = TestUtils.getResourceAsString(filename);
        ResponseEntity<Void> responseEntity = getTestApi()
                .postForEntity(requestJson, "/api/report/create-report", TestUtils.VOID_TYPE);
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);
    }

    protected GetReportDataResponseDto executeGetReportData(String reportId, HttpStatus expectedStatus) {
        ResponseEntity<GetReportDataResponseDto> responseEntity = getTestApi()
                .getForEntity("/api/report/get-report-by-id/" + reportId, GET_REPORT_RESPONSE_TYPE);

        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(expectedStatus);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return responseEntity.getBody();
        }

        return null;
    }
}
