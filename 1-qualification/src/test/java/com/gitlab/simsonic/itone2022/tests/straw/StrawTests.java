package com.gitlab.simsonic.itone2022.tests.straw;

import com.gitlab.simsonic.itone2022.BaseApiTest;
import com.gitlab.simsonic.itone2022.server.straw.StrawController.ExecuteSqlRequestDto;
import com.gitlab.simsonic.itone2022.server.table_queries.service.QueryExecutionService;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Slf4j
public class StrawTests extends BaseApiTest {

    @Autowired
    private QueryExecutionService queryExecutionService;

    @Test
    void testSqlToCSV() {
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);

        queryExecutionService.sqlExecute("INSERT INTO Artists (id)            VALUES (10)");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id, name)      VALUES (11, 'Чувак')");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id, name)      VALUES (12, 'Дылда')");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id, age)       VALUES (13, 13)");
        queryExecutionService.sqlExecute("INSERT INTO Artists (id, name, age) VALUES (14, 'Моряк', 48)");

        String sql = "SELECT * FROM Artists";
        ExecuteSqlRequestDto requestDto = new ExecuteSqlRequestDto(sql);

        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(
                "/api/straw/sql/query",
                requestDto,
                String.class,
                Collections.emptyMap());
        assertThat(responseEntity)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode)
                .isEqualTo(HttpStatus.OK);

        String responseCsv = responseEntity.getBody();
        assertThat(responseCsv)
                .isNotNull()
                .isEqualToIgnoringNewLines("""
                                                   10;;
                                                   11;Чувак;
                                                   12;Дылда;
                                                   13;;13
                                                   14;Моряк;48
                                                   """);
    }
}
