package com.gitlab.simsonic.itone2022.tests.table_queries;

import com.gitlab.simsonic.itone2022.BaseApiTest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CreateQueryTests extends BaseApiTest {

    @Test
    void testSuccess() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);
    }

    @Test
    void testErrors() {
        executeCreateQuery("/tests/table_queries/create-1-error.json", HttpStatus.NOT_ACCEPTABLE);
        executeCreateQuery("/tests/table_queries/create-abc-error.json", HttpStatus.NOT_ACCEPTABLE);
    }

    @Disabled
    @Test
    void testShouldBeErrorButSpecLies() {
        // executeCreateQuery("/tests/table_queries/create-1-error.json", HttpStatus.NOT_ACCEPTABLE);
        executeCreateQuery("/tests/table_queries/create-1-error.json", HttpStatus.CREATED);
        // executeCreateQuery("/tests/table_queries/create-abc-error.json", HttpStatus.NOT_ACCEPTABLE);
        executeCreateQuery("/tests/table_queries/create-abc-error.json", HttpStatus.CREATED);
    }
}
