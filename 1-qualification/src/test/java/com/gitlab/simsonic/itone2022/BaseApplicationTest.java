package com.gitlab.simsonic.itone2022;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.support.TransactionTemplate;

import com.gitlab.simsonic.itone2022.config.ApplicationTestConfig;
import com.gitlab.simsonic.itone2022.utils.TestApi;

import java.util.concurrent.Callable;

@SpringBootTest(
        classes = {
                ItOneApplication.class,
                ApplicationTestConfig.class,
        },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureTestDatabase(
        connection = EmbeddedDatabaseConnection.H2,
        replace = AutoConfigureTestDatabase.Replace.ANY)
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@Slf4j
public class BaseApplicationTest {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Клиент для API, предоставляемого сервером.
     */
    @Getter
    @Autowired
    private TestApi testApi;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    protected void transactional(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> runnable.run());
    }

    protected <V> V transactional(Callable<V> callable) {
        return transactionTemplate.execute(status -> callWithoutTryCatch(callable));
    }

    @SneakyThrows
    private static <V> V callWithoutTryCatch(Callable<V> callable) {
        return callable.call();
    }

    @SneakyThrows
    protected String toJson(Object anyDataObject) {
        return objectMapper.writeValueAsString(anyDataObject);
    }

    @SneakyThrows
    protected <T> T fromJson(String json, Class<T> clazz) {
        return objectMapper.readValue(json, clazz);
    }
}
