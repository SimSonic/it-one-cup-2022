package com.gitlab.simsonic.itone2022.tests.table_queries;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import com.gitlab.simsonic.itone2022.BaseApiTest;
import com.gitlab.simsonic.itone2022.server.table_queries.api.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.utils.TestUtils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GetAndListQueriesTests extends BaseApiTest {

    private static final TypeReference<List<TableQueryResponseDto>> LIST_QUERIES_TYPE_REFERENCE = new TypeReference<>() {};

    @BeforeEach
    void beforeEach() {
        executeCreateTable("/tests/tables/1-create-table-customer.json", HttpStatus.CREATED);
        executeCreateTable("/tests/tables/2-create-table-artists.json", HttpStatus.CREATED);

        executeCreateQuery("/tests/table_queries/create-1-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-2-ok.json", HttpStatus.CREATED);
        executeCreateQuery("/tests/table_queries/create-3-ok.json", HttpStatus.CREATED);
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            1; /tests/table_queries/create-1-ok.json;
            2; /tests/table_queries/create-2-ok.json;
            3; /tests/table_queries/create-3-ok.json;
            """,
            delimiter = ';')
    void testGetById(String queryId, String expectedJson) {
        TableQueryResponseDto result = executeGetQuery(queryId, HttpStatus.OK);
        TableQueryResponseDto expected = TestUtils.getResource(expectedJson, TableQueryResponseDto.class);
        assertThat(result)
                .isNotNull()
                .isEqualTo(expected);
    }

    @Test
    void testGetIncorrectId() {
        TableQueryResponseDto result = executeGetQuery("fake", HttpStatus.NOT_ACCEPTABLE);
        assertThat(result)
                .isNull();
    }

    @Test
    void testGetNonExistingId() {
        TableQueryResponseDto result = executeGetQuery("100500", HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(result)
                .isNull();
    }

    @Test
    void testListArtists() {
        List<TableQueryResponseDto> result = executeListTableQueries("Artists");
        List<TableQueryResponseDto> expected = TestUtils.getResource(
                "/tests/table_queries/list-artists-ok.json",
                LIST_QUERIES_TYPE_REFERENCE);
        assertThat(result)
                .isNotNull()
                .containsExactlyElementsOf(expected);
    }

    @Test
    void testListAll() {
        List<TableQueryResponseDto> result = executeListAllQueries();
        List<TableQueryResponseDto> expected = TestUtils.getResource(
                "/tests/table_queries/list-all-ok.json",
                LIST_QUERIES_TYPE_REFERENCE);
        assertThat(result)
                .isNotNull()
                .containsExactlyElementsOf(expected);
    }

    @Test
    void testListNothing() {
        executeDeleteQuery("1", HttpStatus.ACCEPTED);
        executeDeleteQuery("2", HttpStatus.ACCEPTED);
        executeDeleteQuery("3", HttpStatus.ACCEPTED);

        List<TableQueryResponseDto> result = executeListAllQueries();
        assertThat(result)
                .isNotNull()
                .isEmpty();
    }

    @Test
    void testListCustomer() {
        List<TableQueryResponseDto> result = executeListTableQueries("Customer");
        List<TableQueryResponseDto> expected = TestUtils.getResource(
                "/tests/table_queries/list-customer-ok.json",
                LIST_QUERIES_TYPE_REFERENCE);
        assertThat(result)
                .isNotNull()
                .containsExactlyElementsOf(expected);
    }

    @Test
    void testEmpty() {
        executeDeleteQuery("3", HttpStatus.ACCEPTED);

        List<TableQueryResponseDto> result = executeListTableQueries("Customer");
        assertThat(result)
                .isNotNull()
                .isEmpty();
    }

    @Test
    void testTableIsAbsent() {
        List<TableQueryResponseDto> result = executeListTableQueries("FlyInHighBirdOfPray");
        assertThat(result)
                .isEmpty();
    }
}
