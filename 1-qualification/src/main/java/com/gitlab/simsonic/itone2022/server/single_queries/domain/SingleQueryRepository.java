package com.gitlab.simsonic.itone2022.server.single_queries.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SingleQueryRepository extends JpaRepository<SingleQueryEntity, Long> {

}
