package com.gitlab.simsonic.itone2022.server.table_queries.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import com.gitlab.simsonic.itone2022.server.table_queries.api.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.table_queries.domain.TableQueryEntity;
import com.gitlab.simsonic.itone2022.server.table_queries.domain.TableQueryRepository;
import com.gitlab.simsonic.itone2022.server.tables.service.TableService;

import javax.transaction.Transactional;
import javax.validation.ValidationException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TableQueryService {

    private final TableQueryRepository tableQueryRepository;
    private final TableService tableService;

    @Transactional
    public void createQuery(long queryId, String tableName, String sql) {
        boolean tableIsAbsent = tableService.findTable(tableName).isEmpty();
        if (tableIsAbsent) {
            throw new ValidationException("Таблица запроса %s не существует".formatted(tableName));
        }

        if (tableQueryRepository.existsById(queryId)) {
            throw new ValidationException("Запрос %d уже существует".formatted(queryId));
        }

        TableQueryEntity entity = new TableQueryEntity();
        entity.setId(queryId);
        entity.setTableName(tableName);
        entity.setQuery(sql);

        tableQueryRepository.save(entity);
    }

    @Transactional(dontRollbackOn = ValidationException.class)
    public void modifyQuery(Long queryId, String tableName, String sql) {
        boolean tableIsAbsent = tableService.findTable(tableName).isEmpty();
        if (tableIsAbsent) {
            throw new ValidationException("Таблица запроса %s не существует".formatted(tableName));
        }

        TableQueryEntity entity = tableQueryRepository.findById(queryId)
                .orElseThrow(() -> new ValidationException("Запрос %d не существует".formatted(queryId)));

        entity.setTableName(tableName);
        entity.setQuery(sql);

        tableQueryRepository.save(entity);
    }

    public void deleteTableQueries(String tableName) {
        List<TableQueryResponseDto> tableQueries = ObjectUtils.getIfNull(
                getTableQueries(tableName),
                Collections::emptyList);
        for (TableQueryResponseDto tableQuery : tableQueries) {
            deleteQuery(tableQuery.getQueryId());
        }
    }

    public void deleteQuery(long queryId) {
        if (tableQueryRepository.existsById(queryId)) {
            tableQueryRepository.deleteById(queryId);
            return;
        }

        throw new ValidationException("Нет удаляемого запроса " + queryId);
    }

    @Nullable
    public List<TableQueryResponseDto> getTableQueries(String tableName) {
        boolean tableIsAbsent = tableService.findTable(tableName).isEmpty();
        // Полностью пустой ответ по ТЗ это null или "[]"?!?
        if (tableIsAbsent) {
            return List.of();
        }

        List<TableQueryEntity> tableQueries = tableQueryRepository.findAllByTableNameOrderById(tableName);

        return StreamEx.of(tableQueries)
                .map(TableQueryMapper::map)
                .toList();
    }

    public TableQueryResponseDto getQuery(long queryId) {
        return tableQueryRepository.findById(queryId)
                .map(TableQueryMapper::map)
                .orElse(null);
    }

    public List<TableQueryResponseDto> getAllTableQueries() {
        return tableQueryRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(TableQueryEntity::getId))
                .map(TableQueryMapper::map)
                .toList();
    }
}
