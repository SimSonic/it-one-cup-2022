package com.gitlab.simsonic.itone2022.server.table_queries.api;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TableQueryResponseDto {

    long queryId;
    String tableName;
    String query;
}
