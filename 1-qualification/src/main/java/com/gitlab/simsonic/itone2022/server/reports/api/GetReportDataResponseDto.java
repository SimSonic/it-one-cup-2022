package com.gitlab.simsonic.itone2022.server.reports.api;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class GetReportDataResponseDto {

    long reportId;
    int tableAmount;
    List<ReportDataTable> tables;

}
