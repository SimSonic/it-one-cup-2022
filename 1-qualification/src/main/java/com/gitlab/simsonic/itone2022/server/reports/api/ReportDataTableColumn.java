package com.gitlab.simsonic.itone2022.server.reports.api;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ReportDataTableColumn {

    String title;
    String type;
    int size; // STRING?
}
