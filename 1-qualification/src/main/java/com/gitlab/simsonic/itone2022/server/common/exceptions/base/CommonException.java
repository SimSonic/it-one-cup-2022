package com.gitlab.simsonic.itone2022.server.common.exceptions.base;

import lombok.Getter;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

import java.io.Serial;
import java.util.Objects;

/**
 * Базовое исключение, которое предполагается бросать в приложении.
 */
@Getter
public class CommonException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 2722911635615945119L;

    private final CommonExceptionDescription errorType;

    public CommonException(@NonNull CommonExceptionDescription errorType, String message, Exception cause) {
        super(message, cause);
        this.errorType = Objects.requireNonNull(errorType);
    }

    @Override
    public String getMessage() {
        String defaultMessage = errorType.getDefaultMessage();
        String currentMessage = super.getMessage();
        return StreamEx.of(defaultMessage, currentMessage)
                .map(StringUtils::trimToNull)
                .nonNull()
                .joining(": ");
    }
}
