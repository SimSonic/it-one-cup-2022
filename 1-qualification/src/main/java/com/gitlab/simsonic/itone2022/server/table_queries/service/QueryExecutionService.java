package com.gitlab.simsonic.itone2022.server.table_queries.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.gitlab.simsonic.itone2022.server.single_queries.domain.SingleQueryEntity;
import com.gitlab.simsonic.itone2022.server.single_queries.domain.SingleQueryRepository;
import com.gitlab.simsonic.itone2022.server.table_queries.domain.TableQueryEntity;
import com.gitlab.simsonic.itone2022.server.table_queries.domain.TableQueryRepository;
import com.gitlab.simsonic.itone2022.server.tables.service.TableService;

import javax.validation.ValidationException;

import java.sql.ResultSet;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class QueryExecutionService {

    private final TableQueryRepository tableQueryRepository;
    private final SingleQueryRepository singleQueryRepository;
    private final TableService tableService;
    private final JdbcTemplate jdbcTemplate;

    public void executeTableQuery(long queryId) {
        TableQueryEntity query = tableQueryRepository.findById(queryId)
                .orElseThrow(() -> new ValidationException("Невозможно запустить несуществующий запрос"));

        String tableName = query.getTableName();
        tableService.findTable(tableName)
                .orElseThrow(() -> new ValidationException("Запрос привязан к несуществующей таблице"));

        String sql = query.getQuery();
        sqlExecute(sql);
    }

    public void executeSingleQuery(long queryId) {
        SingleQueryEntity query = singleQueryRepository.findById(queryId)
                .orElseThrow(() -> new ValidationException("Невозможно запустить несуществующий пр.запрос"));

        String sql = query.getQuery();
        sqlExecute(sql);
    }

    @SneakyThrows
    public void sqlExecute(String sql) {
        try {
            log.info("Run SQL: {}", sql);
            jdbcTemplate.execute(sql);
        } catch (Exception ex) {
            throw new SQLSyntaxErrorException("Ошибка запроса: " + ex.getMessage(), ex);
        }
    }

    @SneakyThrows
    public String sqlQueryToCSV(String sql) {
        try {
            log.info("Run SQL to CSV: {}", sql);
            List<String> result = jdbcTemplate.query(sql, QueryExecutionService::resultSetToCsvLine);
            return String.join("\n", result);
        } catch (Exception ex) {
            throw new SQLSyntaxErrorException("Ошибка запроса: " + ex.getMessage(), ex);
        }
    }

    @SneakyThrows
    private static String resultSetToCsvLine(ResultSet rs, int rowNum) {
        int columnCount = rs.getMetaData().getColumnCount();
        Collection<String> result = new ArrayList<>(columnCount);

        for (int idx = 1; idx <= columnCount; idx += 1) {
            result.add(StringUtils.trimToEmpty(rs.getString(idx)));
        }

        return String.join(";", result);
    }
}
