package com.gitlab.simsonic.itone2022.server.straw.featureflags;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class FeatureFlagsService {

    private final Map<FeatureFlags, String> featureFlags = new ConcurrentHashMap<>();

    FeatureFlagsService() {
        for (FeatureFlags ff : FeatureFlags.values()) {
            featureFlags.put(ff, ff.getDefaultValue());
        }
    }

    public Map<FeatureFlags, String> getFeatureFlags() {
        return Collections.unmodifiableMap(featureFlags);
    }

    public boolean isFalse(FeatureFlags featureFlag) {
        return !isTrue(featureFlag);
    }

    public boolean isTrue(FeatureFlags featureFlag) {
        String value = getValue(featureFlag);
        return BooleanUtils.toBoolean(value);
    }

    public String getValue(FeatureFlags featureFlag) {
        return featureFlags.get(featureFlag);
    }

    public void update(FeatureFlags featureFlag, String value) {
        featureFlags.put(featureFlag, value);
    }
}
