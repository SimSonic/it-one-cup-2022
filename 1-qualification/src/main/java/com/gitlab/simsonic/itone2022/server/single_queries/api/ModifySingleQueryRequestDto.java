package com.gitlab.simsonic.itone2022.server.single_queries.api;

import lombok.Builder;
import lombok.Value;

import com.gitlab.simsonic.itone2022.server.utils.NamingUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Value
@Builder
public class ModifySingleQueryRequestDto {

    @NotNull
    @Positive
    Long queryId;

    @NotNull
    @Size(min = 1, max = NamingUtils.MAX_QUERY_LENGTH)
    String query;
}
