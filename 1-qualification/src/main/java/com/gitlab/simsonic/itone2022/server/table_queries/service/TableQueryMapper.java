package com.gitlab.simsonic.itone2022.server.table_queries.service;

import lombok.experimental.UtilityClass;

import com.gitlab.simsonic.itone2022.server.table_queries.api.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.table_queries.domain.TableQueryEntity;

@UtilityClass
public class TableQueryMapper {

    static TableQueryResponseDto map(TableQueryEntity tq) {
        return TableQueryResponseDto.builder()
                .queryId(tq.getId())
                .tableName(tq.getTableName())
                .query(tq.getQuery())
                .build();
    }
}
