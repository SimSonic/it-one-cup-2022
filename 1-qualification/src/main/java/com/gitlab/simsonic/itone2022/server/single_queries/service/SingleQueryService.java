package com.gitlab.simsonic.itone2022.server.single_queries.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import com.gitlab.simsonic.itone2022.server.single_queries.api.SingleQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.single_queries.domain.SingleQueryEntity;
import com.gitlab.simsonic.itone2022.server.single_queries.domain.SingleQueryRepository;

import javax.transaction.Transactional;
import javax.validation.ValidationException;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SingleQueryService {

    private final SingleQueryRepository singleQueryRepository;

    @Transactional
    public void createQuery(long queryId, String sql) {
        if (singleQueryRepository.existsById(queryId)) {
            throw new ValidationException("Пр.запрос %d уже существует".formatted(queryId));
        }

        SingleQueryEntity entity = new SingleQueryEntity();
        entity.setId(queryId);
        entity.setQuery(sql);

        singleQueryRepository.save(entity);
    }

    @Transactional
    public void updateQuery(Long queryId, String sql) {
        SingleQueryEntity entity = singleQueryRepository.findById(queryId)
                .orElseThrow(() -> new ValidationException("Пр.запрос %d не существует".formatted(queryId)));

        entity.setQuery(sql);

        singleQueryRepository.save(entity);
    }

    @Transactional
    public boolean deleteQuery(long queryId) {
        if (singleQueryRepository.existsById(queryId)) {
            singleQueryRepository.deleteById(queryId);
            return true;
        }

        return false;
    }

    public SingleQueryResponseDto getQuery(long queryId) {
        return singleQueryRepository.findById(queryId)
                .map(SingleQueryMapper::map)
                .orElse(null);
    }

    public List<SingleQueryResponseDto> getAllNonTableQueries() {
        return singleQueryRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(SingleQueryEntity::getId))
                .map(SingleQueryMapper::map)
                .toList();
    }
}
