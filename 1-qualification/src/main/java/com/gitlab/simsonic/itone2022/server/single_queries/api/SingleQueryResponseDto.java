package com.gitlab.simsonic.itone2022.server.single_queries.api;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SingleQueryResponseDto {

    long queryId;
    String query;
}
