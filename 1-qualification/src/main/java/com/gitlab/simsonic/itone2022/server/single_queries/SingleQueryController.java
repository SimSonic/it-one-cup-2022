package com.gitlab.simsonic.itone2022.server.single_queries;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.simsonic.itone2022.server.single_queries.api.AddNewSingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.single_queries.api.ModifySingleQueryRequestDto;
import com.gitlab.simsonic.itone2022.server.single_queries.api.SingleQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.single_queries.service.SingleQueryService;
import com.gitlab.simsonic.itone2022.server.table_queries.service.QueryExecutionService;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/single-query")
@Slf4j
public class SingleQueryController {

    private final SingleQueryService singleQueryService;
    private final QueryExecutionService queryExecutionService;

    @PostMapping("/add-new-query")
    @ResponseStatus(HttpStatus.CREATED)
    void createQuery(
            @Valid @RequestBody AddNewSingleQueryRequestDto requestDto
    ) {
        singleQueryService.createQuery(
                requestDto.getQueryId(),
                requestDto.getQuery());
    }

    @PutMapping("/modify-query")
    void modifyQuery(
            @Valid @RequestBody ModifySingleQueryRequestDto requestDto
    ) {
        singleQueryService.updateQuery(
                requestDto.getQueryId(),
                requestDto.getQuery());
    }

    @DeleteMapping("/delete-single-query-by-id/{queryId}")
    ResponseEntity<Void> deleteQuery(
            @Positive
            @PathVariable("queryId") long queryId
    ) {
        boolean result = singleQueryService.deleteQuery(queryId);
        HttpStatus status = result ? HttpStatus.ACCEPTED : HttpStatus.NOT_ACCEPTABLE;
        return ResponseEntity.status(status).build();
    }

    @GetMapping(value = "/execute-single-query-by-id/{queryId}")
    @ResponseStatus(HttpStatus.CREATED)
    void executeQuery(
            @Positive
            @PathVariable("queryId") long queryId
    ) {
        queryExecutionService.executeSingleQuery(queryId);
    }

    @GetMapping(
            value = "/get-single-query-by-id/{queryId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleQueryResponseDto> getQuery(
            @Positive
            @PathVariable("queryId") long queryId
    ) {
        SingleQueryResponseDto result = singleQueryService.getQuery(queryId);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping(
            value = "/get-all-single-queries",
            produces = MediaType.APPLICATION_JSON_VALUE)
    List<SingleQueryResponseDto> getQuery() {
        return singleQueryService.getAllNonTableQueries();
    }
}
