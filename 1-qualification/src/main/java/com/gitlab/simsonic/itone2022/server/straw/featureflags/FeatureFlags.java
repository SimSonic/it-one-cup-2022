package com.gitlab.simsonic.itone2022.server.straw.featureflags;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum FeatureFlags {

    /**
     * На запрос /api/table-query/modify-query-in-table
     * если пришёл слишком длинный SQL (больше 120 символов), перед ответом 406 клиенту
     * очистить поле sql этого запроса в БД.
     */
    EMPTY_TABLE_QUERY_ON_SQL_LIMIT("true"),

    /**
     * При удалении таблицы удалять связанные с ней запросы.
     */
    CASCADE_DELETE_TABLE_QUERIES("true"),

    /**
     * При формировании отчёта учитывать считать в каждой колонке число NOT NULL значений
     * или использовать для этого общее кол-во строк в таблице.
     */
    REPORT_SUM_ONLY_NON_NULL_VALUES("true"),
    ;

    private final String defaultValue;
}
