package com.gitlab.simsonic.itone2022;

import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.gitlab.simsonic.itone2022.config.ItOneProperties;

@EnableConfigurationProperties(value = ItOneProperties.class)
@SpringBootApplication(proxyBeanMethods = false)
public class ItOneApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .sources(ItOneApplication.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }
}
