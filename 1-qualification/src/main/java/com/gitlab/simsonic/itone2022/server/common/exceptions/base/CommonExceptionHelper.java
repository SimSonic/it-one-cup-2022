package com.gitlab.simsonic.itone2022.server.common.exceptions.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.gitlab.simsonic.itone2022.server.common.ErrorResponseDto;
import com.gitlab.simsonic.itone2022.server.common.exceptions.CommonExceptions;

import javax.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

/**
 * Обработка исключений, вылетающих из контроллеров.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class CommonExceptionHelper {

    private final ObjectMapper objectMapper;

    /**
     * Преобразование исключения в DTO для клиента и его непосредственный вывод.
     */
    @SneakyThrows
    public void processCommonExceptionToResponse(HttpServletResponse response, CommonException ex) {
        log.warn("Обработана ошибка: {}", ex.getMessage());

        CommonExceptionDescription description = ex.getErrorType();

        ErrorResponseDto dto = buildErrorResponseAndLogIt(description, ex);

        String contentBody = objectMapper.writeValueAsString(dto);
        byte[] contentBytes = contentBody.getBytes(StandardCharsets.UTF_8);

        // ----- FIXME ----- :
        contentBytes = new byte[0];

        response.setStatus(description.getHttpStatus().value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setContentLength(contentBytes.length);

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(contentBytes)) {
            byteArrayInputStream.transferTo(response.getOutputStream());
        }
    }

    /**
     * Сработала валидация Spring.
     */
    public ErrorResponseDto buildErrorResponse(MethodArgumentNotValidException ex) {
        String message = StreamEx.of(ex.getBindingResult().getFieldErrors())
                .map(fe -> fe.getField() + ' ' + fe.getDefaultMessage())
                .ifEmpty("Некорректное значение.")
                .joining(", ");

        CommonException exception = CommonExceptions.VALIDATION_FAILED.creator()
                .message(message)
                .cause(ex)
                .create();

        // return buildErrorResponseAndLogIt(CommonExceptions.VALIDATION_FAILED, exception);

        // ----- FIXME ----- :

        buildErrorResponseAndLogIt(CommonExceptions.VALIDATION_FAILED, exception);
        return null;
    }

    /**
     * Преобразование исключения в DTO для клиентской стороны (МП / т.п.),
     * и параллельно его логирование в Graylog / мессенджеры, при необходимости.
     */
    public ErrorResponseDto buildErrorResponseAndLogIt(CommonExceptionDescription errorType, Throwable ex) {
        String errorMessage = ex.getMessage();

        log.warn("Ошибка запроса: {}", errorMessage);

        return ErrorResponseDto.builder()
                .errorCode(errorType.getName())
                .errorMessage(errorMessage)
                .build();
    }
}
