package com.gitlab.simsonic.itone2022.server.tables.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.gitlab.simsonic.itone2022.server.straw.featureflags.FeatureFlags;
import com.gitlab.simsonic.itone2022.server.straw.featureflags.FeatureFlagsService;
import com.gitlab.simsonic.itone2022.server.table_queries.service.TableQueryService;
import com.gitlab.simsonic.itone2022.server.tables.api.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.tables.api.CreateTableRequestDto.ColumnInfo;
import com.gitlab.simsonic.itone2022.server.tables.api.GetTableResponseDto;
import com.gitlab.simsonic.itone2022.server.tables.domain.TableEntity;
import com.gitlab.simsonic.itone2022.server.tables.domain.TableRepository;

import javax.validation.ValidationException;

import java.sql.ResultSet;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TableService {

    private final TableRepository tableRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;
    private final FeatureFlagsService featureFlagsService;

    @Lazy
    private final TableQueryService tableQueryService;

    public Optional<TableEntity> findTable(String tableName) {
        return tableRepository.findById(tableName);
    }

    @SneakyThrows
    public void createTable(CreateTableRequestDto requestDto) {
        String tableName = requestDto.getTableName();

        tableRepository.findById(tableName)
                .ifPresent(t -> {
                    throw new ValidationException("Таблица %s уже существует".formatted(tableName));
                });

        String sql = buildCreateTableRequest(requestDto);
        jdbcTemplate.execute(sql);

        TableEntity entity = new TableEntity();
        entity.setTableName(tableName);
        entity.setJson(objectMapper.writeValueAsString(requestDto));
        tableRepository.save(entity);

        log.info("Создана таблица {}", tableName);
    }

    private static String buildCreateTableRequest(CreateTableRequestDto requestDto) {
        var columnInfos = requestDto.getColumnInfos();
        String primaryKey = requestDto.getPrimaryKey();

        String columnList = StreamEx.of(columnInfos)
                .mapToEntry(ColumnInfo::getTitle, ColumnInfo::getType)
                .mapKeyValue((columnName, columnType) -> "%s %s %s".formatted(
                        columnName,
                        columnType,
                        primaryKey.equals(columnName) ? "PRIMARY KEY" : ""))
                .joining(", ");

        return "CREATE TABLE %s (%s)".formatted(requestDto.getTableName(), columnList);
    }

    public GetTableResponseDto getTable(String tableName) {
        return tableRepository.findById(tableName)
                .map(TableEntity::getJson)
                .map(this::fromJson)
                .map(TableMapper::mapTableDescription)
                .orElse(null);
    }

    @SneakyThrows
    private CreateTableRequestDto fromJson(String json) {
        return objectMapper.readValue(json, CreateTableRequestDto.class);
    }

    public HttpStatus dropTable(String tableName) {
        String json = tableRepository.findById(tableName)
                .map(TableEntity::getJson)
                .orElse(null);

        if (json == null) {
            return HttpStatus.NOT_ACCEPTABLE;
        }

        String sql = buildDropTableRequest(tableName);
        jdbcTemplate.execute(sql);

        if (featureFlagsService.isTrue(FeatureFlags.CASCADE_DELETE_TABLE_QUERIES)) {
            tableQueryService.deleteTableQueries(tableName);
        }

        tableRepository.deleteById(tableName);

        return HttpStatus.CREATED;
    }

    private static String buildDropTableRequest(String tableName) {
        return "DROP TABLE %s".formatted(tableName);
    }

    public int getTableRows(String tableName) {
        Integer result = jdbcTemplate.query(
                "SELECT COUNT(*) AS cnt FROM %s".formatted(tableName),
                (ResultSet rs) -> rs.next()
                                  ? rs.getInt("cnt")
                                  : 0);
        Objects.requireNonNull(result);

        return result;
    }

    public int getNonNullColumnRows(String tableName, String columnName) {
        Integer result = jdbcTemplate.query(
                "SELECT COUNT(*) AS cnt FROM %s WHERE %s IS NOT NULL".formatted(tableName, columnName),
                (ResultSet rs) -> rs.next()
                                  ? rs.getInt("cnt")
                                  : 0);
        Objects.requireNonNull(result);

        return result;
    }
}
