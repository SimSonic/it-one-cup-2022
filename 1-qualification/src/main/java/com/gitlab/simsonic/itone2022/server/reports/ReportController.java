package com.gitlab.simsonic.itone2022.server.reports;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.simsonic.itone2022.server.reports.api.CreateReportRequestDto;
import com.gitlab.simsonic.itone2022.server.reports.api.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.server.reports.service.ReportService;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/report")
@Slf4j
public class ReportController {

    private final ReportService reportService;

    @PostMapping("/create-report")
    @ResponseStatus(HttpStatus.CREATED)
    void createReport(
            @Valid @RequestBody CreateReportRequestDto requestDto
    ) {
        reportService.createReport(requestDto);
    }

    @GetMapping(value = "/get-report-by-id/{reportId}")
    @ResponseStatus(HttpStatus.CREATED)
    GetReportDataResponseDto getReportData(
            @Positive
            @PathVariable("reportId") long reportId
    ) {
        return reportService.getReportData(reportId);
    }
}
