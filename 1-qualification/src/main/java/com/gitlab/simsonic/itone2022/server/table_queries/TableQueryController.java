package com.gitlab.simsonic.itone2022.server.table_queries;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.simsonic.itone2022.server.table_queries.api.AddNewQueryToTableRequestDto;
import com.gitlab.simsonic.itone2022.server.table_queries.api.ModifyQueryInTableRequestDto;
import com.gitlab.simsonic.itone2022.server.table_queries.api.TableQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.table_queries.service.QueryExecutionService;
import com.gitlab.simsonic.itone2022.server.table_queries.service.TableQueryService;
import com.gitlab.simsonic.itone2022.server.utils.NamingUtils;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/table-query")
@Slf4j
public class TableQueryController {

    private final TableQueryService tableQueryService;
    private final QueryExecutionService queryExecutionService;

    @PostMapping("/add-new-query-to-table")
    @ResponseStatus(HttpStatus.CREATED)
    void createQuery(
            @Valid @RequestBody AddNewQueryToTableRequestDto requestDto
    ) {
        tableQueryService.createQuery(
                requestDto.getQueryId(),
                requestDto.getTableName(),
                requestDto.getQuery());
    }

    @PutMapping("/modify-query-in-table")
    @ResponseStatus(HttpStatus.OK)
    void modifyQuery(
            @Valid @RequestBody ModifyQueryInTableRequestDto requestDto
    ) {
        tableQueryService.modifyQuery(
                requestDto.getQueryId(),
                requestDto.getTableName(),
                requestDto.getQuery());
    }

    @DeleteMapping("/delete-table-query-by-id/{queryId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void deleteQuery(
            @Positive
            @PathVariable("queryId") long queryId
    ) {
        tableQueryService.deleteQuery(queryId);
    }

    @GetMapping(value = "/execute-table-query-by-id/{queryId}")
    @ResponseStatus(HttpStatus.CREATED)
    void executeQuery(
            @Positive
            @PathVariable("queryId") long queryId
    ) {
        queryExecutionService.executeTableQuery(queryId);
    }

    @GetMapping(
            value = "/get-all-queries-by-table-name/{tableName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<TableQueryResponseDto> listTableQueries(
            @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
            @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
            @PathVariable("tableName") String tableName
    ) {
        return tableQueryService.getTableQueries(tableName);
    }

    @GetMapping(
            value = "/get-table-query-by-id/{queryId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<TableQueryResponseDto> getQuery(
            @Positive
            @PathVariable("queryId") long queryId
    ) {
        TableQueryResponseDto result = tableQueryService.getQuery(queryId);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping(
            value = "/get-all-table-queries",
            produces = MediaType.APPLICATION_JSON_VALUE)
    List<TableQueryResponseDto> getQuery() {
        return tableQueryService.getAllTableQueries();
    }
}
