package com.gitlab.simsonic.itone2022.server.reports.api;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ReportDataTable {

    String tableName;
    List<ReportDataTableColumn> columns;

}
