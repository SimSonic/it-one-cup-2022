package com.gitlab.simsonic.itone2022.server.tables.service;

import lombok.experimental.UtilityClass;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

import com.gitlab.simsonic.itone2022.server.tables.api.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.tables.api.GetTableResponseDto;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@UtilityClass
public class TableMapper {

    private static final Map<String, String> STANDARD_SQL_DATA_TYPES = Map.of(
            "VARCHAR\\(\\d+\\)", "CHARACTER VARYING",
            "VARCHAR", "CHARACTER VARYING",
            "INT1", "TINY INTEGER",
            "INT2", "SMALL INTEGER",
            "INT4", "INTEGER",
            "INT8", "BIG INTEGER"
    );

    static GetTableResponseDto mapTableDescription(CreateTableRequestDto createTableRequestDto) {
        return GetTableResponseDto.builder()
                .tableName(createTableRequestDto.getTableName())
                .columnsAmount(createTableRequestDto.getColumnsAmount())
                .columnInfos(mapColumnInfos(createTableRequestDto.getColumnInfos()))
                .primaryKey(createTableRequestDto.getPrimaryKey().toLowerCase())
                .build();
    }

    private static List<GetTableResponseDto.ColumnInfo> mapColumnInfos(Collection<CreateTableRequestDto.ColumnInfo> columnInfos) {
        return StreamEx.of(columnInfos)
                .map(TableMapper::mapColumnInfo)
                .toList();
    }

    private static GetTableResponseDto.ColumnInfo mapColumnInfo(CreateTableRequestDto.ColumnInfo columnInfo) {
        String columnName = columnInfo.getTitle().toUpperCase();
        String columnType = columnInfo.getType().toUpperCase();

        String standardizedDataType = EntryStream.of(STANDARD_SQL_DATA_TYPES)
                .mapKeys(Pattern::compile)
                .filterKeys(p -> p.matcher(columnType).matches())
                .values()
                .findFirst()
                .orElse(columnType);

        return GetTableResponseDto.ColumnInfo.builder()
                .title(columnName)
                .type(standardizedDataType)
                .build();
    }
}
