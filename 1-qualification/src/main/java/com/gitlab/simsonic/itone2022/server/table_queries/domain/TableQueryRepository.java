package com.gitlab.simsonic.itone2022.server.table_queries.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableQueryRepository extends JpaRepository<TableQueryEntity, Long> {

    List<TableQueryEntity> findAllByTableNameOrderById(String tableName);
}
