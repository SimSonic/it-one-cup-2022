package com.gitlab.simsonic.itone2022.server.single_queries.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "it_one_2022_single_queries")
@Getter
@Setter
public class SingleQueryEntity {

    @Id
    private Long id;

    private String query;
}
