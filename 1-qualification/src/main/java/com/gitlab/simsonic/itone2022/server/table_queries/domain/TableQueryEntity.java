package com.gitlab.simsonic.itone2022.server.table_queries.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(
        name = "it_one_2022_table_queries",
        indexes = @Index(columnList = "tableName"))
@Getter
@Setter
public class TableQueryEntity {

    @Id
    private Long id;

    private String tableName;

    private String query;
}
