package com.gitlab.simsonic.itone2022.server.table_queries.api;

import lombok.Builder;
import lombok.Value;

import com.gitlab.simsonic.itone2022.server.utils.NamingUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Value
@Builder
public class AddNewQueryToTableRequestDto {

    @NotNull
    @Positive
    Long queryId;

    @NotBlank
    @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
    @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
    String tableName;

    @NotNull
    @Size(min = 1, max = NamingUtils.MAX_QUERY_LENGTH)
    String query;
}
