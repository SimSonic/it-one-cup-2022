package com.gitlab.simsonic.itone2022.server.single_queries.service;

import lombok.experimental.UtilityClass;

import com.gitlab.simsonic.itone2022.server.single_queries.api.SingleQueryResponseDto;
import com.gitlab.simsonic.itone2022.server.single_queries.domain.SingleQueryEntity;

@UtilityClass
public class SingleQueryMapper {

    static SingleQueryResponseDto map(SingleQueryEntity tq) {
        return SingleQueryResponseDto.builder()
                .queryId(tq.getId())
                .query(tq.getQuery())
                .build();
    }
}
