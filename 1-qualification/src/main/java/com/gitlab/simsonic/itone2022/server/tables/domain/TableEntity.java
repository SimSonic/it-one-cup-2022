package com.gitlab.simsonic.itone2022.server.tables.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(
        name = "it_one_2022_tables",
        indexes = @Index(columnList = "tableName"))
@Getter
@Setter
public class TableEntity {

    @Id
    private String tableName;

    @Lob
    private String json;
}
