package com.gitlab.simsonic.itone2022.server.reports.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.gitlab.simsonic.itone2022.server.reports.api.CreateReportRequestDto;
import com.gitlab.simsonic.itone2022.server.reports.api.CreateReportRequestDto.ReportTable;
import com.gitlab.simsonic.itone2022.server.reports.api.CreateReportRequestDto.ReportTable.ReportTableColumn;
import com.gitlab.simsonic.itone2022.server.reports.api.GetReportDataResponseDto;
import com.gitlab.simsonic.itone2022.server.reports.api.ReportDataTable;
import com.gitlab.simsonic.itone2022.server.reports.api.ReportDataTableColumn;
import com.gitlab.simsonic.itone2022.server.reports.domain.ReportEntity;
import com.gitlab.simsonic.itone2022.server.reports.domain.ReportRepository;
import com.gitlab.simsonic.itone2022.server.straw.featureflags.FeatureFlags;
import com.gitlab.simsonic.itone2022.server.straw.featureflags.FeatureFlagsService;
import com.gitlab.simsonic.itone2022.server.tables.api.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.tables.domain.TableEntity;
import com.gitlab.simsonic.itone2022.server.tables.service.TableService;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportService {

    private final ReportRepository reportRepository;
    private final TableService tableService;
    private final ObjectMapper objectMapper;

    private final FeatureFlagsService featureFlagsService;

    public void createReport(CreateReportRequestDto requestDto) {
        validate(requestDto);

        Long reportId = requestDto.getReportId();

        ReportEntity reportEntity = new ReportEntity();
        reportEntity.setId(reportId);
        reportEntity.setJson(toJson(requestDto));
        reportRepository.save(reportEntity);
    }

    private void validate(CreateReportRequestDto requestDto) {
        Long reportId = requestDto.getReportId();

        List<ReportTable> tables = requestDto.getTables();
        if (requestDto.getTableAmount() != tables.size()) {
            throw new ValidationException("Не совпадает кол-во таблиц в отчёте #%d".formatted(reportId));
        }

        if (reportRepository.findById(reportId).isPresent()) {
            throw new ValidationException("Отчёт #%d уже существует".formatted(reportId));
        }

        for (ReportTable reportTable : tables) {
            String tableName = reportTable.getTableName();
            Map<String, String> columnNameToTypeMap = getTableColumnDescriptions(tableName);

            List<ReportTableColumn> columns = reportTable.getColumns();
            for (ReportTableColumn reportTableColumn : columns) {
                String columnName = reportTableColumn.getTitle();
                String expectedType = reportTableColumn.getType();

                String actualType = columnNameToTypeMap.get(columnName);
                if (actualType == null) {
                    // Не соответствует чекеру:
                    throw new ValidationException("Нет такой колонки");
                    // continue;
                }

                testColumnType(actualType.toUpperCase(), expectedType.toUpperCase());
            }
        }
    }

    private Map<String, String> getTableColumnDescriptions(String tableName) {
        TableEntity table = tableService
                .findTable(tableName)
                .orElseThrow(() -> new ValidationException("Таблицы %s нет".formatted(tableName)));

        String json = table.getJson();
        CreateTableRequestDto tableDesc = fromJson(json, CreateTableRequestDto.class);

        return StreamEx.of(tableDesc.getColumnInfos())
                .mapToEntry(
                        CreateTableRequestDto.ColumnInfo::getTitle,
                        CreateTableRequestDto.ColumnInfo::getType)
                .toMap();
    }

    private static void testColumnType(String actualType, String expectedType) {
        if (actualType.contains("VARCHAR") && expectedType.contains("VARCHAR")) {
            return;
        }

        if (!actualType.equals(expectedType)) {
            throw new ValidationException("Тип колонки отличается (%s vs %s)".formatted(actualType, expectedType));
        }
    }

    public GetReportDataResponseDto getReportData(long reportId) {
        ReportEntity entity = reportRepository.findById(reportId)
                .orElseThrow(() -> new ValidationException("Отчёт #%d не существует".formatted(reportId)));

        String json = entity.getJson();
        CreateReportRequestDto report = fromJson(json, CreateReportRequestDto.class);
        List<ReportTable> reportTables = report.getTables();

        return GetReportDataResponseDto.builder()
                .reportId(reportId)
                .tableAmount(reportTables.size())
                .tables(map(reportTables))
                .build();
    }

    private List<ReportDataTable> map(Collection<ReportTable> reportTables) {
        return StreamEx.of(reportTables)
                .map(this::map)
                .toList();
    }

    private ReportDataTable map(ReportTable reportTable) {
        String tableName = reportTable.getTableName();

        int tableRows = tableService.getTableRows(tableName);
        Function<String, Integer> fn = featureFlagsService.isTrue(FeatureFlags.REPORT_SUM_ONLY_NON_NULL_VALUES)
                                       ? cn1 -> tableService.getNonNullColumnRows(tableName, cn1)
                                       : cn -> tableRows;

        List<ReportDataTableColumn> columns = StreamEx.of(reportTable.getColumns())
                .map(c -> ReportDataTableColumn.builder()
                        .title(c.getTitle())
                        .type(c.getType())
                        .size(fn.apply(c.getTitle()))
                        .build())
                .toList();

        return ReportDataTable.builder()
                .tableName(tableName)
                .columns(columns)
                .build();
    }

    @SneakyThrows
    protected String toJson(Object anyDataObject) {
        return objectMapper.writeValueAsString(anyDataObject);
    }

    @SneakyThrows
    protected <T> T fromJson(String json, Class<T> clazz) {
        return objectMapper.readValue(json, clazz);
    }
}
