package com.gitlab.simsonic.itone2022.server.tables;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.simsonic.itone2022.server.tables.api.CreateTableRequestDto;
import com.gitlab.simsonic.itone2022.server.tables.api.GetTableResponseDto;
import com.gitlab.simsonic.itone2022.server.tables.service.TableService;
import com.gitlab.simsonic.itone2022.server.utils.NamingUtils;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/table")
@Slf4j
public class TableController {

    private final TableService tableService;

    @PostMapping("/create-table")
    @ResponseStatus(HttpStatus.CREATED)
    void createTable(@Valid @RequestBody CreateTableRequestDto requestDto) {
        validateCreateTable(requestDto);
        tableService.createTable(requestDto);
    }

    private static void validateCreateTable(CreateTableRequestDto requestDto) {
        var columnsAmount = requestDto.getColumnsAmount();
        var columnInfos = requestDto.getColumnInfos();
        if (columnsAmount != columnInfos.size()) {
            throw new ValidationException("Некорректное значение columnsAmount");
        }

        String primaryKey = requestDto.getPrimaryKey();
        StreamEx.of(columnInfos)
                .map(CreateTableRequestDto.ColumnInfo::getTitle)
                .findFirst(primaryKey::equals)
                .orElseThrow(() -> new ValidationException("Некорректный PK."));
    }

    @GetMapping(
            value = "/get-table-by-name/{tableName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    GetTableResponseDto getTableByName(
            @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
            @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
            @PathVariable("tableName") String tableName
    ) {
        return tableService.getTable(tableName);
    }

    @DeleteMapping("/drop-table-by-name/{tableName}")
    ResponseEntity<Void> dropTable(
            @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
            @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
            @PathVariable("tableName") String tableName
    ) {
        HttpStatus status = tableService.dropTable(tableName);
        return ResponseEntity.status(status).build();
    }

    @DeleteMapping("/drop-table/{tableName}")
    ResponseEntity<Void> dropTableByName(
            @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
            @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
            @PathVariable("tableName") String tableName
    ) {
        HttpStatus status = tableService.dropTable(tableName);
        return ResponseEntity.status(status).build();
    }
}
