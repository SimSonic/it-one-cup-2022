package com.gitlab.simsonic.itone2022.server.reports.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "it_one_2022_reports")
@Getter
@Setter
public class ReportEntity {

    @Id
    private Long id;

    @Lob
    private String json;
}
