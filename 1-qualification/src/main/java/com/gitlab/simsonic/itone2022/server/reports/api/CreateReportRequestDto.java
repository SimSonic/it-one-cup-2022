package com.gitlab.simsonic.itone2022.server.reports.api;

import lombok.Builder;
import lombok.Value;

import com.gitlab.simsonic.itone2022.server.utils.NamingUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import java.util.List;

@Value
@Builder
public class CreateReportRequestDto {

    @NotNull
    @Positive
    Long reportId;

    @NotNull
    @PositiveOrZero
    Integer tableAmount;

    @NotNull
    List<@Valid ReportTable> tables;

    @Value
    @Builder
    public static class ReportTable {

        @NotBlank
        @Size(min = 1, max = NamingUtils.MAX_TABLE_NAME)
        @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
        String tableName;

        @NotNull
        List<@Valid ReportTableColumn> columns;

        @Value
        @Builder
        public static class ReportTableColumn {

            @NotBlank
            @Pattern(regexp = NamingUtils.IDENTIFIER_PATTERN)
            String title;

            @NotBlank
            String type;
        }
    }
}
