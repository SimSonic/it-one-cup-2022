package com.gitlab.simsonic.itone2022.server.straw;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.simsonic.itone2022.server.straw.featureflags.FeatureFlags;
import com.gitlab.simsonic.itone2022.server.straw.featureflags.FeatureFlagsService;
import com.gitlab.simsonic.itone2022.server.table_queries.service.QueryExecutionService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import java.util.Map;

/**
 * Немного соломки.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/straw")
@Slf4j
public class StrawController {

    private final QueryExecutionService queryExecutionService;
    private final FeatureFlagsService featureFlagsService;

    @Value
    public static class ExecuteSqlRequestDto {

        @NotBlank String sql;
    }

    @PostMapping("/sql/query")
    String querySql(
            @Valid @RequestBody ExecuteSqlRequestDto requestDto
    ) {
        return queryExecutionService.sqlQueryToCSV(requestDto.getSql());
    }

    @PostMapping("/sql/execute")
    void executeSql(
            @Valid @RequestBody ExecuteSqlRequestDto requestDto
    ) {
        queryExecutionService.sqlExecute(requestDto.getSql());
    }

    @GetMapping("/feature-flags")
    Map<FeatureFlags, String> getFeatureFlags() {
        return featureFlagsService.getFeatureFlags();
    }

    @PostMapping("/feature-flags/{featureFlag}/{value}")
    void writeFeatureFlagValue(
            @PathVariable("featureFlag") FeatureFlags featureFlag,
            @PathVariable("value") String value
    ) {
        featureFlagsService.update(featureFlag, value);
    }
}
